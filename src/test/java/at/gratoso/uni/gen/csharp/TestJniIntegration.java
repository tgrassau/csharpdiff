package at.gratoso.uni.gen.csharp;

import net.sf.jni4net.Bridge;
import org.junit.Test;
import system.collections.IDictionary;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class TestJniIntegration {
    @Test
    public void testSystemCall() throws Exception {
        Bridge.setVerbose(true);
        Bridge.init();

        // get environment variables
        IDictionary variables =  system.Environment.GetEnvironmentVariables();

        assertNotNull(variables);
        assertNotEquals(0, variables.getCount());
    }

    @Test
    public void testRoslynIntegration() throws Exception {
        Bridge.init();
        Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("lib/RoslynWrapper.j4n.dll"));

        String text = "class Foo { void Bar() { } }";

        Object tree = new roslynwrapper.CShparSyntaxTreeGenerator().GenerateTree(text);

        assertNotNull(tree);
    }
}
