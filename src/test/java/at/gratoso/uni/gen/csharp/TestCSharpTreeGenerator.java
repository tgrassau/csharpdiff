package at.gratoso.uni.gen.csharp;

import com.github.gumtreediff.tree.ITree;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestCSharpTreeGenerator {
    @Test
    public void testSimpleSyntax() throws Exception {
        String text = "class Foo { void Bar() { throw new NotImplementedException(); } }";

        ITree target = new CSharpTreeGenerator().generateFromString(text).getRoot();

        assertNotNull(target);
    }

    @Test
    public void testLambdaExpressions() throws Exception {
        String text =
                "using System;" +
                "namespace test {" +
                "   class Foo { public void Bar() { Func<int> test = () => 17; } }" +
                "}";

        ITree target = new CSharpTreeGenerator().generateFromString(text).getRoot();

        assertNotNull(target);
    }

    @Test
    public void iterateOverNodes() throws Exception {
        String text =
                "class Foo { protected int Prop { get; set; } public void Bar() { throw new NotImplementedException(); } }";

        ITree target = new CSharpTreeGenerator().generateFromString(text).getRoot();

        for(ITree ctx : target.getDescendants()) {
            assertNotNull(ctx);
        }
    }

    @Test
    public void identifierNodesAreInserted() throws Exception {
        String text =
                "class Foo { protected int Prop { get; set; } public void Bar() { throw new NotImplementedException(); } }";

        ITree target = new CSharpTreeGenerator().generateFromString(text).getRoot();

        for(ITree node : target.getDescendants()) {
            if (RoslynWrapperHelper.nodeHasIdentifier(node))
            {
                int identifierCount = 0;

                for (ITree child : node.getChildren()) {
                    if (child.getType() == SyntaxKind.IdentifierToken.getValue())
                        identifierCount++;
                }
                assertEquals(1, identifierCount);
            }
        }
    }

    @Test
    public void test1() throws Exception {
        String text = "public class Foo { public void bar(int input, Foo input1) {  } }";
        ITree target = new CSharpTreeGenerator().generateFromString(text).getRoot();

        String text1 = "public class Foo { public void bar(Foo input1, int input) {  } }";
        ITree target1 = new CSharpTreeGenerator().generateFromString(text).getRoot();

        assertNotNull(target);
        assertNotNull(target1);
    }
}
