package at.gratoso.uni.changeClassifier;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class TestClassifyUpdate extends TestClassifyBase {

    @Test
    public void testStatementUpdateInStaticContext() throws Exception {
        String srcText = createClassSource("internal static readonly EdmModelValidationRule<NavigationProperty>\n" +
                "            EdmNavigationProperty_ResultEndMustNotBeNull =\n" +
                "                new EdmModelValidationRule<NavigationProperty>(\n" +
                "                    (context, edmNavigationProperty) =>\n" +
                "                        {\n" +
                "                            if (edmNavigationProperty.ResultEnd == null)\n" +
                "                            {\n" +
                "                                context.AddError(\n" +
                "                                    edmNavigationProperty,\n" +
                "                                    XmlConstants.ToRole,\n" +
                "                                    Strings.EdmModel_Validator_Syntactic_EdmNavigationProperty_ResultEndMustNotBeNull);\n" +
                "                            }\n" +
                "                        }\n" +
                "                    );");

        String dstText = createClassSource("internal static readonly EdmModelValidationRule<NavigationProperty>\n" +
                "            EdmNavigationProperty_ResultEndMustNotBeNull =\n" +
                "                new EdmModelValidationRule<NavigationProperty>(\n" +
                "                    (context, edmNavigationProperty) =>\n" +
                "                        {\n" +
                "                            if (edmNavigationProperty.ToEndMember == null)\n" +
                "                            {\n" +
                "                                context.AddError(\n" +
                "                                    edmNavigationProperty,\n" +
                "                                    XmlConstants.ToRole,\n" +
                "                                    Strings.EdmModel_Validator_Syntactic_EdmNavigationProperty_ResultEndMustNotBeNull);\n" +
                "                            }\n" +
                "                        }\n" +
                "                    );");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.CONDITION_EXPRESSION_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testUpdateChainedExpression() throws Exception {
        String srcText = createClassSource(
                "public void Foo() {" +
                        "   var aset = databaseMapping.Model.Containers[0].AssociationSets[0];" +
                        "}"
        );

        String dstText = createClassSource(
                "public void Foo() {" +
                        "   var aset = databaseMapping.Model.Containers.Single().AssociationSets[0];" +
                        "}"
        );

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.STATEMENT_UPDATE, target.get(0).getChangeType());
    }

    @Test
    public void testUpdateWithAwait() throws Exception{
        String srcText = createClassSource(
                "public void Foo() {" +
                "   using (var e = source.GetAsyncEnumerator())" +
                "   {" +
                "       while (await e.MoveNextAsync(cancellationToken).ConfigureAwait(continueOnCapturedContext: false)) " +
                "       {" +
                "           cancellationToken.ThrowIfCancellationRequested();" +
                "       }" +
                "   }" +
                "}"
        );

        String dstText = createClassSource(
                "public void Foo() {" +
                "   using (var e = source.GetAsyncEnumerator())" +
                "   {" +
                "       while (await e.MoveNextAsync(cancellationToken).WithCurrentCulture()) " +
                "       {" +
                "           cancellationToken.ThrowIfCancellationRequested();" +
                "       }" +
                "   }" +
                "}"
        );

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.CONDITION_EXPRESSION_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testUpdateNodeReference() throws Exception {
        String srcText = createClassSource("public int Foo() { return 17; }");
        String dstText = createClassSource("public int FooNew() { return 17; }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals("FooNew", target.get(0).getNode().getLabel());
    }

    @Test
    public void testAttributeUpdate() throws Exception {
        String srcText = createClassSource(
                "         [Fact]\n" +
                        "         public void Verify_DbContext_construction_for_SQLCE_when_constructed_via_DbCompiledModel_ctor()\n" +
                        "         {\n" +
                        "             Verify_DbContext_construction_for_SQLCE(DbContextConstructorArgumentType.DbCompiledModel);\n" +
                        "         }");

        String dstText = createClassSource(
                "         [ExtendedFact(SkipForLocalDb = true, SkipForSqlAzure = true)]\n" +
                        "         public void Verify_DbContext_construction_for_SQLCE_when_constructed_via_DbCompiledModel_ctor()\n" +
                        "         {\n" +
                        "             Verify_DbContext_construction_for_SQLCE(DbContextConstructorArgumentType.DbCompiledModel);\n" +
                        "         }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ATTRIBUTE_UPDATE, target.get(0).getChangeType());
    }

    @Test
    public void testInheritanceChange() throws IOException {
        String srcText = "namespace test { class Foo : Bar { } }";
        String dstText = "namespace test { class Foo : SuperBar { } }";

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARENT_TYPE_CHANGE, 1));
    }

    @Test
    public void testClassNameChange() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test { class Foo1 { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.CLASS_RENAMING, 1));
    }

    @Test
    public void testAddClassTypeParameter() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test { class Foo<T> { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(!containsChange(target, ChangeType.CLASS_RENAMING));
    }

    @Test
    public void testMethodNameChange() throws IOException {
        String srcText = "namespace test { class Foo { public void Bar() {} } }";
        String dstText = "namespace test { class Foo { public void Bar1() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.METHOD_RENAMING, 1));
    }

    @Test
    public void testFieldNameChange() throws IOException {
        String srcText = "namespace test { class Foo { private int _test; public void Bar() {} } }";
        String dstText = "namespace test { class Foo { private int _test1; public void Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.ATTRIBUTE_RENAMING, 1));
    }

    @Test
    public void testMethodParameterRenaming() throws IOException {
        // Change.Type.ChangeType.PARAMETER_RENAMING

        String srcText = "namespace test { class Foo { public void Bar(int i) {} } }";
        String dstText = "namespace test { class Foo { public void Bar(int j) {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARAMETER_RENAMING, 1));
    }

    @Test
    public void testMethodParameterTypeChange() throws IOException {
        // ChangeType.PARAMETER_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public void Bar(double i) {} } }";
        String dstText = "namespace test { class Foo { public void Bar(int i) {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARAMETER_TYPE_CHANGE, 1));
    }

    @Test
    public void testMethodParameterTypeChangeWithCustomClass() throws IOException {
        // ChangeType.PARAMETER_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public void Bar(double i) {} } }";
        String dstText = "namespace test { class Foo { public void Bar(Foo i) {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARAMETER_TYPE_CHANGE, 1));
    }

    @Test
    public void testMuliptleMethodParameterChanges() throws IOException {
        // ChangeType.PARAMETER_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public void Bar(Foo x) {} } }";
        String dstText = "namespace test { class Foo { public void Bar(Foo y, int z) {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(containsChange(target, ChangeType.PARAMETER_RENAMING, 1));
        assertTrue(containsChange(target, ChangeType.PARAMETER_INSERT, 1));
    }

    @Test
    public void testReturnParameterDelete() throws IOException {
        // ChangeType.RETURN_TYPE_DELETE ... old type -> void

        String srcText = "namespace test { class Foo { public int Bar() {} } }";
        String dstText = "namespace test { class Foo { public void Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.RETURN_TYPE_DELETE, 1));
    }

    @Test
    public void testReturnParameterInsert() throws IOException {
        // ChangeType.RETURN_TYPE_INSERT ... void -> new type

        String srcText = "namespace test { class Foo { public void Bar() {} } }";
        String dstText = "namespace test { class Foo { public int Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.RETURN_TYPE_INSERT, 1));
    }

    @Test
    public void testReturnParameterRemove() throws IOException {
        // ChangeType.RETURN_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public short Bar() {} } }";
        String dstText = "namespace test { class Foo { public float Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.RETURN_TYPE_CHANGE, 1));
    }

    @Test
    public void testTypeParameterInSignatureIsIgnored() throws IOException {
        // ChangeType.RETURN_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public short Bar() {} } }";
        String dstText = "namespace test { class Foo { public short Bar<T>() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(0, target.size());
    }

    @Test
    public void testTypeParameterConstraintInSignatureIsIgnored() throws IOException {
        // ChangeType.RETURN_TYPE_CHANGE ... old type -> new type

        String srcText = "namespace test { class Foo { public short Bar() {} } }";
        String dstText = "namespace test { class Foo { public float Bar<T>() where T : class {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.RETURN_TYPE_CHANGE, 1));
    }

    @Test
    public void testAttributeTypeChange() throws IOException {
        String srcText = "namespace test { class Foo { private int _test; } }";
        String dstText = "namespace test { class Foo { private long _test; } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.ATTRIBUTE_TYPE_CHANGE, 1));
    }

    @Test
    public void testAttributeTypeChange1() throws IOException {
        String srcText = "namespace test { class Foo { private Foo _test; } }";
        String dstText = "namespace test { class Foo { private long _test; } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.ATTRIBUTE_TYPE_CHANGE, 1));
    }

    @Test
    public void testAttributeTypeChange2() throws IOException {
        String srcText = "namespace test { class Foo { private int _test; } }";
        String dstText = "namespace test { class Foo { private int _test = 1000; } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(0, target.size());
    }

    @Test
    public void testIfConditionChange() throws IOException {
        String srcText = "namespace test { class Foo { public short Bar() { if (1 == 2) { } } } }";
        String dstText = "namespace test { class Foo { public short Bar() { if (1 == 1 || i == 8) { } } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(containsChange(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testForConditionChange() throws IOException {
        String srcText = "namespace test { class Foo { public short Bar() { while (1 == 2) { } } } }";
        String dstText = "namespace test { class Foo { public short Bar() { while (1 == 1 || i == 8) { } } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(containsChange(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testWhileConditionChange() throws IOException {
        String srcText = "namespace test { class Foo { public short Bar() { for (i = 0; 1 == 1; i++) { } } } }";
        String dstText = "namespace test { class Foo { public short Bar() { for (i = 0; i < 1000; i++) { } } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(containsChange(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }
}
