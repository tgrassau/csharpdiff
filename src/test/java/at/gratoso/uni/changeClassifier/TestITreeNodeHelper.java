package at.gratoso.uni.changeClassifier;

import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.gen.csharp.CSharpTreeGenerator;
import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.tree.ITree;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import static org.junit.Assert.assertEquals;

public class TestITreeNodeHelper {

    @Test
    public void canGetInnerClassName() throws IOException {
        String code = "namespace test { class A : B { class C : D { } } }";
        ITree tree = GetTree(code);

        List<ITree> nodes = getAllOfType(tree, SyntaxKind.ClassDeclaration);

        assertEquals(2, nodes.size());
        assertEquals(ITreeNodeHelper.getUniqueNodeName(nodes.get(0)), "test.A");
        assertEquals(ITreeNodeHelper.getUniqueNodeName(nodes.get(1)), "test.A.C");
    }

    @Test
    public void canGetHethodNameWithRefParam() throws IOException {
        String code = "namespace blubb { public class Foo { internal override ObjectQueryExecutionPlan GetExecutionPlan(MergeOption? forMergeOption) { return; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.MethodDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.GetExecutionPlan(MergeOption)", target);
    }

    @Test
    public void canGetHethodNameWithOutParam() throws IOException {
        String code = "namespace blubb { public class Foo { public void Bar(a, out b) { return; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.MethodDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Bar(a,)", target);
    }

    @Test
    public void canGetHethodNameWithDefault() throws IOException {
        String code = "namespace blubb { public class Foo { public bool Bar(String a, double b, Foo<T> c, int[] d, Foo<int> e = null) { return true; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.MethodDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Bar(String,double,Foo<>,int[],Foo<>)", target);
    }

    @Test
    public void canGetMethodName() throws IOException {
        String code = "namespace blubb { public class Foo { public bool Bar(String a, double b, Foo<T> c, int[] d, Foo<int> e) { return true; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.MethodDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Bar(String,double,Foo<>,int[],Foo<>)", target);
    }

    @Test
    public void canGetConstructorName() throws IOException {
        String code = "namespace blubb { public class Foo { public Foo() { } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.ConstructorDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Foo()", target);
    }

    @Test
    public void canGetConstructorName1() throws IOException {
        String code = "namespace blubb { public class Foo { public Foo(int i) { } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.ConstructorDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Foo(int)", target);
    }

    @Test
    public void canGetPropertyName() throws IOException {
        String code = "namespace blubb { public class Foo { public int Bar { get; set; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.PropertyDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo.Bar", target);
    }

    @Test
    public void canGetFieldName() throws IOException {
        String code = "namespace blubb { public class Foo { private bool _test; } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.FieldDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo._test", target);
    }

    @Test
    public void canGetClassName() throws IOException {
        String code = "namespace blubb { public class Foo { public bool Bar() { return true; } } }";
        ITree tree = GetTree(code);

        ITree node = findFirstOfType(tree, SyntaxKind.ClassDeclaration);
        String target = ITreeNodeHelper.getUniqueNodeName(node);

        assertEquals("blubb.Foo", target);
    }

    @Test
    public void canGetTopLevelNode() throws IOException {
        String code = "namespace blubb { public class Foo { public bool Bar() { return true; } } }";
        ITree tree = GetTree(code);

        ITree classNode = findFirstOfType(tree, SyntaxKind.ClassDeclaration);
        ITree methodNode = findFirstOfType(tree, SyntaxKind.MethodDeclaration);

        assertEquals(classNode, ITreeNodeHelper.getTopLevelNode(methodNode));
    }

    @Test
    public void canGetTopLevelNode1() throws IOException {
        String code = "namespace blubb { public class Foo { public bool Bar() { return true; } } }";
        ITree tree = GetTree(code);

        ITree classNode = findFirstOfType(tree, SyntaxKind.ClassDeclaration);

        assertEquals(classNode, ITreeNodeHelper.getTopLevelNode(classNode));
    }

    @Test
    public void canGetNameWithComplexNamespace() throws IOException {
        String code = "namespace Gratoso.Test.Blubb { public class Foo { public void Bar() { int i = 17; i--; if (i == 16) { i++; } return; } } }";

        ITree tree = GetTree(code);
        ITree classNode = findFirstOfType(tree, SyntaxKind.ClassDeclaration);

        String className = ITreeNodeHelper.getUniqueNodeName(classNode);
        assertEquals("Gratoso.Test.Blubb.Foo", className);
        for (ITree child : classNode.getChildren()) {
            assertEquals(
                    className,
                    ITreeNodeHelper.getUniqueNodeName(ITreeNodeHelper.getTopLevelNode(child)));
        }
    }

    private ITree findFirstOfType(ITree tree, SyntaxKind kind) {
        if (tree.getType() == kind.getValue())
            return tree;

        ITree retVal = null;

        for(ITree child : tree.getChildren()) {
            retVal = findFirstOfType(child, kind);

            if (retVal != null)
                break;
        }

        return retVal;
    }

    private List<ITree> getAllOfType(ITree tree, SyntaxKind kind) {
        List<ITree> retVal = new Vector<>();

        if (tree.getType() == kind.getValue())
            retVal.add(tree);

        for(ITree child : tree.getDescendants()) {
            if (child.getType() == kind.getValue())
                retVal.add(child);
        }

        return retVal;
    }

    private ITree GetTree(String code) throws IOException {
        CSharpTreeGenerator generator = new CSharpTreeGenerator();
        return generator.generateFromString(code).getRoot();
    }
}
