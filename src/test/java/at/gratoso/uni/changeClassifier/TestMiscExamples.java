package at.gratoso.uni.changeClassifier;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class TestMiscExamples extends TestClassifyBase {


    @Test
    public void testStatementInsert() throws Exception {
        String srcText = createClassSource("[Fact]\n" +
                "public void Throws_if_no_store_connection_set()\n" +
                "{\n" +
                "    var entityStateManagerMock = new Mock<ObjectStateManager>();\n" +
                "    var entityStateEntryMock = new Mock<ObjectStateEntry>();\n" +
                "    entityStateManagerMock.Setup(m => m.GetObjectStateEntriesInternal(It.IsAny<EntityState>()))\n" +
                "        .Returns(new[] { entityStateEntryMock.Object });\n" +
                "\n" +
                "    var mockContext = new Mock<ObjectContext>(null, null, null, null);\n" +
                "    mockContext.Setup(m => m.ObjectStateManager).Returns(entityStateManagerMock.Object);\n" +
                "\n" +
                "    var entityAdapter = new EntityAdapter(mockContext.Object);\n" +
                "\n" +
                "    var entityConnectionMock = new Mock<EntityConnection>();\n" +
                "    entityConnectionMock.Setup(m => m.StoreProviderFactory)\n" +
                "        .Returns(new Mock<DbProviderFactory>().Object);\n" +
                "    entityAdapter.Connection = entityConnectionMock.Object;\n" +
                "\n" +
                "    Assert.Equal(\n" +
                "        Strings.EntityClient_NoStoreConnectionForUpdate,\n" +
                "        Assert.Throws<InvalidOperationException>(\n" +
                "            () => entityAdapter.UpdateAsync(CancellationToken.None)).Message);\n" +
                "}");

        String dstText = createClassSource("[Fact]\n" +
                "public void Throws_if_no_store_connection_set()\n" +
                "{\n" +
                "    var entityStateManagerMock = new Mock<ObjectStateManager>();\n" +
                "    entityStateManagerMock.Setup(m => m.HasChanges()).Returns(true);\n" +
                "    var entityStateEntryMock = new Mock<ObjectStateEntry>();\n" +
                "    entityStateManagerMock.Setup(m => m.GetObjectStateEntriesInternal(It.IsAny<EntityState>()))\n" +
                "        .Returns(new[] { entityStateEntryMock.Object });\n" +
                "\n" +
                "    var mockContext = new Mock<ObjectContext>(null, null, null, null);\n" +
                "    mockContext.Setup(m => m.ObjectStateManager).Returns(entityStateManagerMock.Object);\n" +
                "\n" +
                "    var entityAdapter = new EntityAdapter(mockContext.Object);\n" +
                "\n" +
                "    var entityConnectionMock = new Mock<EntityConnection>();\n" +
                "    entityConnectionMock.Setup(m => m.StoreProviderFactory)\n" +
                "        .Returns(new Mock<DbProviderFactory>().Object);\n" +
                "    entityAdapter.Connection = entityConnectionMock.Object;\n" +
                "\n" +
                "    Assert.Equal(\n" +
                "        Strings.EntityClient_NoStoreConnectionForUpdate,\n" +
                "        Assert.Throws<InvalidOperationException>(\n" +
                "            () => entityAdapter.UpdateAsync(CancellationToken.None)).Message);\n" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
    }

    @Test
    public void testMethodInsert1() throws Exception {
        String srcText = createClassSource("[Fact]\n" +
                "public void A()\n" +
                "{\n" +
                "    var migrationSqlGenerator = new SqlServerMigrationSqlGenerator();\n" +
                "\n" +
                "    var column = new ColumnModel(PrimitiveTypeKind.String)\n" +
                "    {\n" +
                "        Name = \"Bar\",\n" +
                "        StoreType = \"varchar\",\n" +
                "        MaxLength = 15\n" +
                "        };\n" +
                "    var addColumnOperation = new AddColumnOperation(\"Foo\", column);\n" +
                "\n" +
                "    var sql = migrationSqlGenerator.Generate(new[] { addColumnOperation }, \"2008\").Join(s => s.Sql, Environment.NewLine);\n" +
                "\n" +
                "    Assert.Contains(\"ALTER TABLE [Foo] ADD [Bar] [varchar](15)\", sql);\n" +
                "}");

        String dstText = createClassSource("[Fact]\n" +
                "public void A()\n" +
                "{\n" +
                "    var migrationSqlGenerator = new SqlServerMigrationSqlGenerator();\n" +
                "\n" +
                "    var column = new ColumnModel(PrimitiveTypeKind.String)\n" +
                "    {\n" +
                "        Name = \"Bar\",\n" +
                "        StoreType = \"varchar\"\n" +
                "    };\n" +
                "    var addColumnOperation = new AddColumnOperation(\"Foo\", column);\n" +
                "\n" +
                "    var sql = migrationSqlGenerator.Generate(new[] { addColumnOperation }, \"2008\").Join(s => s.Sql, Environment.NewLine);\n" +
                "\n" +
                "    Assert.Contains(\"ALTER TABLE [Foo] ADD [Bar] [varchar](8000)\", sql);\n" +
                "}\n" +
                "\n" +
                "[Fact]\n" +
                "public void B()\n" +
                "{\n" +
                "    var migrationSqlGenerator = new SqlServerMigrationSqlGenerator();\n" +
                "\n" +
                "    var column = new ColumnModel(PrimitiveTypeKind.String)\n" +
                "                     {\n" +
                "                         Name = \"Bar\",\n" +
                "                         StoreType = \"varchar\",\n" +
                "                         MaxLength = 15\n" +
                "                     };\n" +
                "    var addColumnOperation = new AddColumnOperation(\"Foo\", column);\n" +
                "\n" +
                "    var sql = migrationSqlGenerator.Generate(new[] { addColumnOperation }, \"2008\").Join(s => s.Sql, Environment.NewLine);\n" +
                "\n" +
                "    Assert.Contains(\"ALTER TABLE [Foo] ADD [Bar] [varchar](15)\", sql);\n" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.ADDITIONAL_FUNCTIONALITY));
        assertEquals(1, getChangeCount(target, ChangeType.METHOD_RENAMING));
    }

    @Test
    public void TestConstructorStatementChange() throws IOException {
        String srcText = createClassSource(" " +
                "private string _schema; " +
                "private string _name; " +
                "public Foo(string name, string schema) " +
                "{ " +
                "   DebugCheck.NotEmpty(name); " +
                "   _name = name; " +
                "   _schema = schema; " +
                "}");

        String dstText = createClassSource(" " +
                "private string _schema; " +
                "private string _name; " +
                "public Foo(string name, string schema) " +
                "{ " +
                "   _name = name; " +
                "   _schema = !string.IsNullOrEmpty(schema) ? schema : null; " +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(3, target.size());
        assertEquals(2, getChangeCount(target, ChangeType.STATEMENT_DELETE));
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
    }

    @Test
    public void testExtractMethod() throws IOException {
        String srcText = createClassSource("public virtual EdmType BaseType\n" +
                "        {\n" +
                "            get { return _baseType; }\n" +
                "            internal set\n" +
                "            {\n" +
                "                Util.ThrowIfReadOnly(this);\n" +
                "\n" +
                "                // Check to make sure there won't be a loop in the inheritance\n" +
                "                var type = value;\n" +
                "                while (type != null)\n" +
                "                {\n" +
                "                    Debug.Assert(type != this, \"Cannot set the given type as base type because it would introduce a loop in inheritance\");\n" +
                "\n" +
                "                    type = type.BaseType;\n" +
                "                }\n" +
                "\n" +
                "                // Also if the base type is EntityTypeBase, make sure it doesn't have keys\n" +
                "                Debug.Assert(\n" +
                "                    value == null ||\n" +
                "                    !Helper.IsEntityTypeBase(this) ||\n" +
                "                    ((EntityTypeBase)this).KeyMembers.Count == 0 ||\n" +
                "                    ((EntityTypeBase)value).KeyMembers.Count == 0,\n" +
                "                    \" For EntityTypeBase, both base type and derived types cannot have keys defined\");\n" +
                "\n" +
                "                _baseType = value;\n" +
                "            }\n" +
                "        }");

        String dstText = createClassSource("public virtual EdmType BaseType\n" +
                "        {\n" +
                "            get { return _baseType; }\n" +
                "            internal set\n" +
                "            {\n" +
                "                Util.ThrowIfReadOnly(this);\n" +
                "\n" +
                "                CheckBaseType(value);\n" +
                "\n" +
                "                _baseType = value;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        private void CheckBaseType(EdmType baseType)\n" +
                "        {\n" +
                "            for (var type = baseType; type != null; type = type.BaseType)\n" +
                "            {\n" +
                "                if (type == this)\n" +
                "                {\n" +
                "                    throw new ArgumentException(Strings.CannotSetBaseTypeCyclicInheritance(baseType.Name, Name));\n" +
                "                }\n" +
                "            }\n" +
                "\n" +
                "            if (baseType != null\n" +
                "                && Helper.IsEntityTypeBase(this)\n" +
                "                && ((EntityTypeBase)baseType).KeyMembers.Count != 0\n" +
                "                && ((EntityTypeBase)this).KeyMembers.Count != 0)\n" +
                "            {\n" +
                "                throw new ArgumentException(Strings.CannotDefineKeysOnBothBaseAndDerivedTypes);\n" +
                "            }\n" +
                "        }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
    }

    @Test
    public void testExtractMethod1() throws IOException {
        String srcText = createClassSource("public virtual void SetBaseType(BaseType baseType)\n" +
                "{\n" +
                "\tUtil.ThrowIfReadOnly(this);\n" +
                "\n" +
                "\tfor (var type = baseType; type != null; type = type.BaseType)\n" +
                "\t{\n" +
                "\t\tif (type == this)\n" +
                "\t\t{\n" +
                "\t\t\tthrow new ArgumentException(Strings.CannotSetBaseTypeCyclicInheritance(baseType.Name, Name));\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tif (baseType != null\n" +
                "\t\t&& Helper.IsEntityTypeBase(this)\n" +
                "\t\t&& ((EntityTypeBase)baseType).KeyMembers.Count != 0\n" +
                "\t\t&& ((EntityTypeBase)this).KeyMembers.Count != 0)\n" +
                "\t{\n" +
                "\t\tthrow new ArgumentException(Strings.CannotDefineKeysOnBothBaseAndDerivedTypes);\n" +
                "\t}\n" +
                "\n" +
                "\t_baseType = baseType;\n" +
                "}");

        String dstText = createClassSource("public virtual void SetBaseType(BaseType baseType)\n" +
                "{\n" +
                "\tUtil.ThrowIfReadOnly(this);\n" +
                "\n" +
                "\tCheckBaseType(baseType);\n" +
                "\n" +
                "\t_baseType = baseType;\n" +
                "}\n" +
                "\n" +
                "private void CheckBaseType(EdmType baseType)\n" +
                "{\n" +
                "\tfor (var type = baseType; type != null; type = type.BaseType)\n" +
                "\t{\n" +
                "\t\tif (type == this)\n" +
                "\t\t{\n" +
                "\t\t\tthrow new ArgumentException(Strings.CannotSetBaseTypeCyclicInheritance(baseType.Name, Name));\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\n" +
                "\tif (baseType != null\n" +
                "\t\t&& Helper.IsEntityTypeBase(this)\n" +
                "\t\t&& ((EntityTypeBase)baseType).KeyMembers.Count != 0\n" +
                "\t\t&& ((EntityTypeBase)this).KeyMembers.Count != 0)\n" +
                "\t{\n" +
                "\t\tthrow new ArgumentException(Strings.CannotDefineKeysOnBothBaseAndDerivedTypes);\n" +
                "\t}\n" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.ADDITIONAL_FUNCTIONALITY));
    }

    @Test
    public void TestAssignmentUpdateTest() throws IOException {
        String srcText = createClassSource("public void Bar() {" +
                        "   connection =\n" +
                        "       DbProviderServices.GetProviderFactory(objectContext.Connection.StoreConnection).CreateConnection();\n" +
                        "}");

        String dstText = createClassSource("public void Bar() {" +
                        "   connection = connection ?? objectContext.Connection.StoreConnection;\n" +
                        "   connection = DbProviderServices.GetProviderServices(connection).CloneDbConnection(connection);" +
                        "}");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(2, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void TestConditionExpressionChangeInConstructor() throws IOException {
        String srcText = createClassSource("public Foo() {" +
                "   var existingConnection = usersContext.Connection;" +
                "   if (existingConnection != null \n" +
                "       && existingConnection.State == ConnectionState.Open)" +
                "   {\n" +
                "       _existingConnection = existingConnection;\n" +
                "   }\n" +
                "}");

        String dstText = createClassSource("public Foo() {" +
                "   var existingConnection = usersContext.Connection;" +
                "   if (existingConnection != null)" +
                "   {\n" +
                "       _existingConnection = existingConnection;\n" +
                "   }\n" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void TestConditionExpressionChangeInMethod() throws IOException {
        String srcText = createClassSource("public void Bar() {" +
                "   var existingConnection = usersContext.Connection;" +
                "   if (existingConnection != null \n" +
                "       && existingConnection.State == ConnectionState.Open)" +
                "   {\n" +
                "       _existingConnection = existingConnection;\n" +
                "   }\n" +
                "}");

        String dstText = createClassSource("public void Bar() {" +
                "   var existingConnection = usersContext.Connection;" +
                "   if (existingConnection != null)" +
                "   {\n" +
                "       _existingConnection = existingConnection;\n" +
                "   }\n" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void TestUsingStatementUpdate() throws IOException {
        String srcText = createClassSource(
                        "public void Bar() {"+
                        "   try\n" +
                        "   {\n" +
                        "       using (var masterConnection = GetProviderFactory(sqlConnection).CreateConnection())\n" +
                        "       {\n" +
                        "           DbInterception.Dispatch.Connection.SetConnectionString(masterConnection,\n" +
                        "               new DbConnectionPropertyInterceptionContext<string>().WithValue(connectionBuilder.ConnectionString));\n" +
                        "       }" +
                        "   }" +
                        "}");

        String dstText = createClassSource(
                "public void Bar() {"+
                "   try\n" +
                        "   {\n" +
                        "       using (var masterConnection = CloneDbConnection(sqlConnection))\n" +
                        "       {\n" +
                        "           DbInterception.Dispatch.Connection.SetConnectionString(masterConnection,\n" +
                        "               new DbConnectionPropertyInterceptionContext<string>().WithValue(connectionBuilder.ConnectionString));\n" +
                        "       }" +
                        "   }" +
                        "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void TestLambdaChange() throws IOException {
        String srcText = createClassSource("public void Bar() { " +
                "             var repository = new EdmMetadataRepository(\"Database=Foo\", SqlClientFactory.Instance);\n" +
                "             var mockContext = CreateMockContext(\"Hash1\", \"Hash2\", \"Hash3\");\n" +
                "             Assert.Equal(\"Hash3\", repository.QueryForModelHash(c => mockContext.Object));" +
                "}");

        String dstText = createClassSource("public void Bar() {" +
                "             var repository = new EdmMetadataRepository(\"Database=Foo\", SqlClientFactory.Instance);\n" +
                "             var mockContext = CreateMockContext(\"Hash1\", \"Hash2\", \"Hash3\");\n" +
                "             Assert.Equal(\"Hash3\", repository.QueryForModelHash(() => mockContext.Object));" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(2, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
    }

    @Test
    public void testConditionExpressionBug() throws IOException {
        String srcText = createClassSource("private ObjectResult<TElement> ExecuteStoreQueryInternal<TElement>(" +
                "string commandText, string entitySetName, ExecutionOptions executionOptions, params object[] parameters)" +
                "{" +
                " if (command != null)" +
                " {" +
                "  command.Dispose();" +
                " }" +
                "}");

        String dstText = createClassSource("private ObjectResult<TElement> ExecuteStoreQueryInternal<TElement>(" +
                "string commandText, string entitySetName, ExecutionOptions executionOptions, params object[] parameters)" +
                "{" +
                " if (command != null)" +
                " {" +
                "  command.Parameters.Clear();" +
                "  command.Dispose();" +
                " }" +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_INSERT));
    }

    @Test
    public void testConstructorClassificationBug() throws IOException {
        String srcText = "// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.\n" +
                "\n" +
                "namespace System.Data.Entity.Internal\n" +
                "{\n" +
                "    using System.Data.Common;\n" +
                "    using System.Data.Entity.Infrastructure.Interception;\n" +
                "    using System.Data.Entity.Utilities;\n" +
                "\n" +
                "    internal abstract class RepositoryBase\n" +
                "    {\n" +
                "        private readonly DbConnection _existingConnection;\n" +
                "        private readonly string _connectionString;\n" +
                "        private readonly DbProviderFactory _providerFactory;\n" +
                "\n" +
                "        protected RepositoryBase(InternalContext usersContext, string connectionString, DbProviderFactory providerFactory)\n" +
                "        {\n" +
                "            DebugCheck.NotNull(usersContext);\n" +
                "            DebugCheck.NotEmpty(connectionString);\n" +
                "            DebugCheck.NotNull(providerFactory);\n" +
                "\n" +
                "            var existingConnection = usersContext.Connection;\n" +
                "            if (existingConnection != null\n" +
                "                && existingConnection.State == ConnectionState.Open)\n" +
                "            {\n" +
                "                _existingConnection = existingConnection;\n" +
                "            }\n" +
                "\n" +
                "            _connectionString = connectionString;\n" +
                "            _providerFactory = providerFactory;\n" +
                "        }\n" +
                "\n" +
                "        protected DbConnection CreateConnection()\n" +
                "        {\n" +
                "            if (_existingConnection != null)\n" +
                "            {\n" +
                "                return _existingConnection;\n" +
                "            }\n" +
                "\n" +
                "            var connection = _providerFactory.CreateConnection();\n" +
                "            DbInterception.Dispatch.Connection.SetConnectionString(connection,\n" +
                "                new DbConnectionPropertyInterceptionContext<string>().WithValue(_connectionString));\n" +
                "\n" +
                "            return connection;\n" +
                "        }\n" +
                "\n" +
                "        protected void DisposeConnection(DbConnection connection)\n" +
                "        {\n" +
                "            if (connection != null && _existingConnection == null)\n" +
                "            {\n" +
                "                DbInterception.Dispatch.Connection.Dispose(connection, new DbInterceptionContext());\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}\n";

        String dstText = "// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.\n" +
                "\n" +
                "namespace System.Data.Entity.Internal\n" +
                "{\n" +
                "    using System.Data.Common;\n" +
                "    using System.Data.Entity.Core.Common;\n" +
                "    using System.Data.Entity.Infrastructure.Interception;\n" +
                "    using System.Data.Entity.Utilities;\n" +
                "\n" +
                "    internal abstract class RepositoryBase\n" +
                "    {\n" +
                "        private readonly DbConnection _existingConnection;\n" +
                "        private readonly string _connectionString;\n" +
                "        private readonly DbProviderFactory _providerFactory;\n" +
                "\n" +
                "        protected RepositoryBase(InternalContext usersContext, string connectionString, DbProviderFactory providerFactory)\n" +
                "        {\n" +
                "            DebugCheck.NotNull(usersContext);\n" +
                "            DebugCheck.NotEmpty(connectionString);\n" +
                "            DebugCheck.NotNull(providerFactory);\n" +
                "\n" +
                "            var existingConnection = usersContext.Connection;\n" +
                "            if (existingConnection != null)\n" +
                "            {\n" +
                "                _existingConnection = existingConnection;\n" +
                "            }\n" +
                "\n" +
                "            _connectionString = connectionString;\n" +
                "            _providerFactory = providerFactory;\n" +
                "        }\n" +
                "\n" +
                "        protected DbConnection CreateConnection()\n" +
                "        {\n" +
                "            if (_existingConnection != null\n" +
                "                && _existingConnection.State == ConnectionState.Open)\n" +
                "            {\n" +
                "                return _existingConnection;\n" +
                "            }\n" +
                "\n" +
                "            var connection = _existingConnection == null\n" +
                "                ? _providerFactory.CreateConnection()\n" +
                "                : DbProviderServices.GetProviderServices(_existingConnection).CloneDbConnection(_existingConnection, _providerFactory);\n" +
                "            DbInterception.Dispatch.Connection.SetConnectionString(connection,\n" +
                "                new DbConnectionPropertyInterceptionContext<string>().WithValue(_connectionString));\n" +
                "\n" +
                "            return connection;\n" +
                "        }\n" +
                "\n" +
                "        protected void DisposeConnection(DbConnection connection)\n" +
                "        {\n" +
                "            if (connection != null && _existingConnection == null)\n" +
                "            {\n" +
                "                DbInterception.Dispatch.Connection.Dispose(connection, new DbInterceptionContext());\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}\n";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(2, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_UPDATE));
    }
}
