package at.gratoso.uni.changeClassifier;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class TestClassifyMove extends TestClassifyBase {

    @Test
    public void testMethodParameterMove() throws IOException {
        String srcText = "namespace test { class Foo { void Bar(int i, double y) { } } }";
        String dstText = "namespace test { class Foo { void Bar(double y, int i) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARAMETER_ORDERING_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testMethodParameterMove1() throws IOException {
        String srcText = "namespace test { class Foo { void Bar(Foo asdf, Bar y) { } } }";
        String dstText = "namespace test { class Foo { void Bar(Bar y, Foo asdf) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARAMETER_ORDERING_CHANGE, target.get(0).getChangeType());
        }

    @Test
    public void testMethodParameterMove2() throws IOException {
        String srcText = "namespace test { class Foo { void Bar(Foo foo, int y) { } } }";
        String dstText = "namespace test { class Foo { void Bar(int y, Foo foo) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARAMETER_ORDERING_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testStatementOrderChanged() throws IOException {
        String srcText = "namespace test { class Foo { void Bar() { int i; int y; } } }";
        String dstText = "namespace test { class Foo { void Bar() { int y; int i; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.STATEMENT_ORDERING_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testStatementOrderChanged1() throws IOException {
        String srcText = "namespace test { class Foo { void Bar() { int i,y = 0; y = 3; if (i == y) { } } } }";
        String dstText = "namespace test { class Foo { void Bar() { int i,y = 0; if (i == y) { } y = 3; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.STATEMENT_ORDERING_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testStatementParentChanged() throws IOException {
        String srcText = "namespace test { class Foo { void Bar() { int i,y = 0; y = 3; if (i == y) { int hallo = 1000; } } } }";
        String dstText = "namespace test { class Foo { void Bar() { int i,y = 0; if (i == y) { int hallo = 1000; y = 3; } } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.STATEMENT_PARENT_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testStatementParentChanged1() throws IOException {
        // TODO: investigate issue with moves

        String srcText = "namespace test { class Foo { int Bar() { return 16+1; } } }";
        String dstText = "namespace test { class Foo { int Bar() { if (33=18) return 16+1; return 0; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, this.getChangeCount(target, ChangeType.STATEMENT_PARENT_CHANGE));
    }


    @Test
    public void testConstructorParameterMove() throws IOException {
        String srcText = "namespace test { class Foo { Foo(int i, int y) { } } }";
        String dstText = "namespace test { class Foo { Foo(int y, int i) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARAMETER_ORDERING_CHANGE, target.get(0).getChangeType());
    }
}
