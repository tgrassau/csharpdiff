package at.gratoso.uni.changeClassifier;

import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.gen.csharp.CSharpTreeGenerator;
import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.tree.TreeContext;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class TestClassifyInsert extends TestClassifyBase {

    @Test
    public void testUpdateOfStaticAssignmentUpdate() throws Exception {
        String srcText = createClassSource(" private static string _test = StaticClass.FunctionCall(); ");
        String dstText = createClassSource(" private static string _test = StaticClass.FunctionCall(17); ");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void testOperatorInsert1() throws Exception{
        String srcText = createClassSource("");
        String dstText = createClassSource("public static Complex operator +(Complex c1, Complex c2)\n" +
                "    {\n" +
                "        Return new Complex(c1.real + c2.real, c1.imaginary + c2.imaginary);\n" +
                "    }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.OPERATOR_INSERT));
    }

    @Test
    public void testOperatorInsert() throws Exception {
        String srcText = createClassSource("");
        String dstText = createClassSource("public static explicit operator TimeSpan?(JToken value) " +
                "{ " +
                "   return null; " +
                "}");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.OPERATOR_INSERT));
    }

    @Test
    public void testInnerClassInsert() throws Exception {
        String srcText = createClassSource("");
        String dstText = createClassSource("public class A : BaseClass { }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_CLASS, target.get(0).getChangeType());
        assertEquals("test.Foo.A", ITreeNodeHelper.getUniqueNodeName(target.get(0).getTopLevelNode()));
    }

    @Test
    public void testAttributeInsert() throws Exception {
        String srcText = createClassSource("public void Test() { }");
        String dstText = createClassSource("[TestAttribute(\"qwer\", Foo = \"asdf\"] public void Test() { }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ATTRIBUTE_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testNamespacesAreIgnored() throws Exception {
        String srcText = "";
        String dstText = "namespace test.a.b { }";

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(0, target.size());
    }

    @Test
    public void testNamespacesAreIgnored1() throws Exception {
        String srcText = "";
        String dstText = "namespace test { }";

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(0, target.size());
    }

    @Test
    public void testFieldModifierChange() throws Exception {
        String srcText = createClassSource("private const string EFSectionName = \"entityFramework\"");
        String dstText = createClassSource("public const string EFSectionName = \"entityFramework\"");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testStaticFieldInsert() throws Exception {
        String srcText = createClassSource(""); // empty class
        String dstText = createClassSource("private static DbContextInfo _currentInfo;");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testUsingInsert() throws Exception {
        String srcText = "namespace test { using System.Data.Common; class Foo { } }";
        String dstText = "namespace test { using System.Data.Common; using System.Data.Entity.Core.Common; class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.USING_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testUsingInsert1() throws Exception {
        String srcText = "using System.Data.Common; namespace test { class Foo { } }";
        String dstText = "using System.Data.Common; using System.Data.Entity.Core.Common; namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.USING_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testCSharpGeneratorCall() throws Exception {
        TreeGenerator gen = new CSharpTreeGenerator();
        String text = "class Foo { void Bar() { } }";
        TreeContext test = gen.generateFromString(text);
        assertNotNull(test);
    }

    @Test
    public void testClassInsert() throws IOException {
        String srcText = "";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_CLASS, target.get(0).getChangeType());
    }

    @Test
    public void testClassInsert1() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test { class Foo { } class Bar { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_CLASS, target.get(0).getChangeType());
    }

    @Test
    public void testClassInheritanceInsert() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test { class Foo : Bar { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARENT_TYPE_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testClassModifierInsert() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test { public class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testClassModifierChange() throws IOException {
        String srcText = "namespace test { private class Foo { } }";
        String dstText = "namespace test { public class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testClassModifierChange1() throws IOException {
        String srcText = "namespace test { public class Foo { } }";
        String dstText = "namespace test { private class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testConstructorInsert() throws IOException {
        String srcText = "namespace test { public class Foo { } }";
        String dstText = "namespace test { public class Foo { public Foo() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_FUNCTIONALITY, target.get(0).getChangeType());
    }

    @Test
    public void testConstructorModifierInsert() throws IOException {
        String srcText = "namespace test { public class Foo { Foo() { } } }";
        String dstText = "namespace test { public class Foo { public Foo() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testConstructorModifierChange() throws IOException {
        String srcText = "namespace test { public class Foo { public Foo() { } } }";
        String dstText = "namespace test { public class Foo { protected Foo() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyInsert() throws IOException {
        String srcText = "namespace test { public class Foo { } }";
        String dstText = "namespace test { public class Foo { int Bar { get; set; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyModifierChange() throws IOException {
        String srcText = "namespace test { public class Foo { int Bar { get; set; } } }";
        String dstText = "namespace test { public class Foo { public int Bar { get; set; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyModifierChange1() throws IOException {
        String srcText = "namespace test { public class Foo { public int Bar { get; set; } } }";
        String dstText = "namespace test { public class Foo { protected int Bar { get; set; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyModifierChange2() throws IOException {
        String srcText = "namespace test { public class Foo { private int Bar { get; set; } } }";
        String dstText = "namespace test { public class Foo { protected int Bar { get; set; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyModifierChange3() throws IOException {
        String srcText = "namespace test { public class Foo { public int Bar { get; set; } } }";
        String dstText = "namespace test { public class Foo { public int Bar { get; private set; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testFieldInsert() throws IOException {
        String srcText = "namespace test { public class Foo {  } }";
        String dstText = "namespace test { public class Foo { int _bar; } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testMethodInsert() throws IOException {
        String srcText = "namespace test { public class Foo {  } }";
        String dstText = "namespace test { public class Foo { public void Bar<T>(int input) where T : class { var i = 1000; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ADDITIONAL_FUNCTIONALITY, target.get(0).getChangeType());
        assertEquals(0, getChangeCount(target, ChangeType.INCREASING_ACCESSIBILITY_CHANGE));
    }

    @Test
    public void testMethodModfierChange() throws IOException {
        String srcText = "namespace test { public class Foo { public void Bar(int input) { } } }";
        String dstText = "namespace test { public class Foo { private void Bar(int input) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testMethodModfierChange1() throws IOException {
        String srcText = "namespace test { public class Foo { public void Bar(int input) { } } }";
        String dstText = "namespace test { public class Foo { protected void Bar(int input) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testMethodModfierChange2() throws IOException {
        String srcText = "namespace test { public class Foo { protected void Bar(int input) { } } }";
        String dstText = "namespace test { public class Foo { protected internal void Bar(int input) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.DECREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testMethodModfierChange3() throws IOException {
        String srcText = "namespace test { public class Foo { private void Bar(int input) { } } }";
        String dstText = "namespace test { public class Foo { internal void Bar(int input) { } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.INCREASING_ACCESSIBILITY_CHANGE, target.get(0).getChangeType());
    }

    @Test
    public void testInheritanceInsert() throws IOException
    {
        String srcText = "namespace test { public class Foo {  } }";
        String dstText = "namespace test { public class Foo : Bar {  } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.PARENT_TYPE_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testInheritanceInsert1() throws IOException
    {
        String srcText = "namespace test { public class Foo : Bar<int> {  } }";
        String dstText = "namespace test { public class Foo : Bar<int>, IBar1 {  } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(ChangeType.PARENT_TYPE_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testStatementInsert() throws IOException {
        String srcText = "namespace test { public class Foo { public void Bar(int input) { } } }";
        String dstText = "namespace test { public class Foo { public void Bar(int input) { int i = 17; } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.STATEMENT_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testElseInsert() throws IOException {
        String srcText = "namespace test { public class Foo { public void Bar(int input) { int i = 17; if (i == 17) { } } } }";
        String dstText = "namespace test { public class Foo { public void Bar(int input) { int i = 17; if (i == 17) { } else { }} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.ALTERNATIVE_PART_INSERT));
    }

    @Test
    public void testCondidtionExpressionChange() throws IOException {
        String srcText = createClassSource("public void Bar() { if (1 == 1) return; }");
        String dstText = createClassSource("public void Bar() { if (1 == 1 && 7 == 3) return; }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testCondidtionExpressionChange1() throws IOException {
        String srcText = createClassSource("public void Bar() { while (1 == 1) return; }");
        String dstText = createClassSource("public void Bar() { while (1 == 1 && 7 == 3) return; }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testCondidtionExpressionChange2() throws IOException {
        String srcText = createClassSource("public void Bar() { for (int i = 0; 1 == 1; i++) return; }");
        String dstText = createClassSource("public void Bar() { for (int i = 0; 1 == 1 && 7 == 3; i++) return; }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testCondidtionExpressionChange3() throws IOException {
        String srcText = "namespace test { class Foo { public short Bar() { do { } while (i < 1000); } } }";
        String dstText = "namespace test { class Foo { public short Bar() { do { } while (i < 1000 && x > y); } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.CONDITION_EXPRESSION_CHANGE));
    }

    @Test
    public void testOverloadChange() throws IOException {
        String srcText = createClassSource("public void Foo() { var tmp = new Temp(1, 2); }");
        String dstText = createClassSource("public void Foo() { var tmp = new Temp(1, 2, 3); }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void testOverloadChange1() throws IOException {
        String srcText = createClassSource("public void Foo() { var tmp = new Temp(1, 2); }");
        String dstText = createClassSource("public void Foo() { var tmp = new Temp(1, 2, 3, a); }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void TestPropertyChange() throws IOException {
        String srcText = createClassSource(" public int Bar { get; set; }");
        String dstText = createClassSource(" public int Bar { get { return 17; } set; }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_INSERT));
    }

    @Test
    public void TestPropertyAddSetter() throws IOException {
        String srcText = createClassSource(" public int Bar { get { return _test; } }");
        String dstText = createClassSource(" public int Bar { get { return _test; } set { _test = value; } }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.ADDITIONAL_FUNCTIONALITY));
    }
}
