package at.gratoso.uni.changeClassifier;


import at.gratoso.uni.gen.csharp.CSharpTreeGenerator;
import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.tree.TreeContext;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

public abstract class TestClassifyBase {
    protected List<SourceCodeChange> classify(String srcString, String dstString) throws IOException {
        return classify(srcString, dstString, false);
    }

    protected List<SourceCodeChange> classify(String srcString, String dstString, boolean includeUnclassified) throws IOException {
        TreeGenerator gen = new CSharpTreeGenerator();

        TreeContext src = gen.generateFromString(srcString);
        TreeContext dst = gen.generateFromString(dstString);

        CSharpChangeClassifier classifier = new CSharpChangeClassifier(includeUnclassified);
        return new Vector<>(classifier.classify(src, dst));
    }

    protected static boolean containsChange(List<SourceCodeChange> changes, ChangeType type) {
        return getChangeCount(changes, type) > 0;
    }

    protected static boolean containsChange(List<SourceCodeChange> changes, ChangeType type, int expectedCount) {
        return getChangeCount(changes, type) == expectedCount;
    }

    protected static int getChangeCount(List<SourceCodeChange> changes, ChangeType type) {
        int changeCount = 0;
        for(SourceCodeChange c : changes) {
            if (c.getChangeType() == type)
                changeCount++;
        }

        return changeCount;
    }

    protected String createClassSource(String classContent) {
        return String.format("namespace test { public class Foo { %s } }", classContent);
    }
}
