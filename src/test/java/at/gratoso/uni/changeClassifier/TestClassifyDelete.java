package at.gratoso.uni.changeClassifier;

import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class TestClassifyDelete extends TestClassifyBase {

    @Test
    public void testUpdateOfStaticAssignmentUpdate() throws Exception {
        String srcText = createClassSource(" private static string _test = StaticClass.FunctionCall(17); ");
        String dstText = createClassSource(" private static string _test = StaticClass.FunctionCall(); ");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(1, getChangeCount(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void testOperatorDelete() throws Exception {
        String srcText = createClassSource("public static explicit operator TimeSpan?(JToken value) " +
                "{ " +
                "   return null; " +
                "}");
        String dstText = createClassSource("");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, getChangeCount(target, ChangeType.OPERATOR_DELETE));
    }

    @Test
    public void testAttributeUpdate() throws Exception {
        String srcText = createClassSource(
                "         [ExtendedFact(SkipForLocalDb = true, SkipForSqlAzure = true)]\n" +
                "         public void Verify_DbContext_construction_for_SQLCE_when_constructed_via_DbCompiledModel_ctor()\n" +
                "         {\n" +
                "             Verify_DbContext_construction_for_SQLCE(DbContextConstructorArgumentType.DbCompiledModel);\n" +
                "         }");

        String dstText = createClassSource(
                "         [ExtendedFact(SkipForLocalDb = true)]\n" +
                "         public void Verify_DbContext_construction_for_SQLCE_when_constructed_via_DbCompiledModel_ctor()\n" +
                "         {\n" +
                "             Verify_DbContext_construction_for_SQLCE(DbContextConstructorArgumentType.DbCompiledModel);\n" +
                "         }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ATTRIBUTE_UPDATE, target.get(0).getChangeType());
    }

    @Test
    public void testAttributeDelete() throws Exception {
        String srcText = createClassSource("[TestAttribute(\"qwer\", Foo = \"asdf\"] public void Test() { }");
        String dstText = createClassSource("public void Test() { }");

        List<SourceCodeChange> target = classify(srcText, dstText, true);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ATTRIBUTE_DELETE, target.get(0).getChangeType());
    }

    @Test
    public void testUsingRemove() throws Exception {
        String srcText = "namespace test { using System.Data.Common; using System.Data.Entity.Core.Common; class Foo { } }";
        String dstText = "namespace test { using System.Data.Common; class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.USING_DELETE, target.get(0).getChangeType());
    }

    @Test
    public void testUsingRemove1() throws Exception {
        String srcText = "using System.Data.Common; namespace test { class Foo { } }";
        String dstText = "using System.Data.Common; using System.Data.Entity.Core.Common; namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.USING_INSERT, target.get(0).getChangeType());
    }

    @Test
    public void testClassRemove() throws IOException {
        String srcText = "namespace test { class Foo { } }";
        String dstText = "namespace test {  }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_CLASS, target.get(0).getChangeType());
    }

    @Test
    public void testClassRemove1() throws IOException {
        String srcText = "namespace test { class Foo { } class Bar { } }";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_CLASS, target.get(0).getChangeType());
    }

    @Test
    public void testClassRemove2() throws IOException {
        String srcText = "namespace test { class Foo { int Test { get; set; } public void Bar() {} } }";
        String dstText = "namespace test {  }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.REMOVED_CLASS, 1));
    }

    @Test
    public void testInnerClassRemove() throws IOException {
        String srcText = "namespace test { class Foo { class Bar { } } }";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertTrue(containsChange(target, ChangeType.REMOVED_CLASS, 1));
    }

    @Test
    public void testMethodRemove() throws IOException {
        String srcText = "namespace test { class Foo { public void Bar() {} } }";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_FUNCTIONALITY, target.get(0).getChangeType());
    }

    @Test
    public void testPropertyRemove() throws IOException {
        String srcText = "namespace test { class Foo { public int Bar { get; set; } } }";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testFieldRemove() throws IOException {
        String srcText = "namespace test { class Foo { private string _test; public void Bar() {} } }";
        String dstText = "namespace test { class Foo { public void Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testFieldRemove1() throws IOException {
        String srcText = "namespace test { class Foo { private string _test = \"ASDF\"; public void Bar() {} } }";
        String dstText = "namespace test { class Foo { public void Bar() {} } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.REMOVED_OBJECT_STATE, target.get(0).getChangeType());
    }

    @Test
    public void testElseRemove() throws IOException {
        String srcText = "namespace test { class Foo { public void Bar() { if (true) {} else {} } } }";
        String dstText = "namespace test { class Foo { public void Bar() { if (true) {} } } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertEquals(ChangeType.ALTERNATIVE_PART_DELETE, target.get(0).getChangeType());
    }

    @Test
    public void testOverloadChange() throws IOException {
        String srcText = createClassSource("public void Foo() { var tmp = new Temp(1, 2, 3); }");
        String dstText = createClassSource("public void Foo() { var tmp = new Temp(1, 2); }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void testOverloadChange1() throws IOException {
        String srcText = createClassSource("public void Foo() { var tmp = new Temp(1, 2, 3, (a + b) * 17); }");
        String dstText = createClassSource("public void Foo() { var tmp = new Temp(1, 2); }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_UPDATE));
    }

    @Test
    public void testRemoveBaseType() throws IOException {
        String srcText = "namespace test { class Foo : Bar { } }";
        String dstText = "namespace test { class Foo { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARENT_TYPE_DELETE));
    }

    @Test
    public void testRemoveBaseType1() throws IOException {
        String srcText = "namespace test { class Foo : Bar, IDoSomeThing { } }";
        String dstText = "namespace test { class Foo : Bar { } }";

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.PARENT_TYPE_DELETE));
    }

    @Test
    public void TestPropertyChange() throws IOException {
        String srcText = createClassSource(" public int Bar { get { int i = 1; return 1; } set { _test = value; } }");
        String dstText = createClassSource(" public int Bar { get { return 1; } set { _test = value; } }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.STATEMENT_DELETE));
    }

    @Test
    public void TestPropertyRemoveSetter() throws IOException {
        String srcText = createClassSource(" public int Bar { get { return _test; } set { _test = value; } }");
        String dstText = createClassSource(" public int Bar { get { return _test; } }");

        List<SourceCodeChange> target = classify(srcText, dstText);

        assertNotNull(target);
        assertEquals(1, target.size());
        assertTrue(containsChange(target, ChangeType.REMOVED_FUNCTIONALITY));
    }
}
