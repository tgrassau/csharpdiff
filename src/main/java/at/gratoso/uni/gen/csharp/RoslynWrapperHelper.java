package at.gratoso.uni.gen.csharp;

import com.github.gumtreediff.tree.ITree;

public class RoslynWrapperHelper {
    public static boolean nodeHasIdentifier(ITree node) {
        return node.getType() == SyntaxKind.ClassDeclaration.getValue()
                || node.getType() == SyntaxKind.DelegateDeclaration.getValue()
                || node.getType() == SyntaxKind.EventDeclaration.getValue()
                || node.getType() == SyntaxKind.EnumDeclaration.getValue()
                || node.getType() == SyntaxKind.InterfaceDeclaration.getValue()
                || node.getType() == SyntaxKind.PropertyDeclaration.getValue();
    }
}
