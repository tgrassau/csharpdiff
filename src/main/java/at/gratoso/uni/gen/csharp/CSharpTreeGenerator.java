package at.gratoso.uni.gen.csharp;

import com.github.gumtreediff.gen.Register;
import com.github.gumtreediff.gen.TreeGenerator;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;
import net.sf.jni4net.Bridge;
import net.sf.jni4net.inj.IClrProxy;
import net.sf.jni4net.jni.IJvmProxy;
import roslynwrapper.CShparSyntaxTreeGenerator;
import roslynwrapper.SyntaxNodeWrapper;
import roslynwrapper.SyntaxTokenWrapper;
import roslynwrapper.SyntaxTreeWrapper;
import system.Type;
import system.collections.IEnumerable;
import system.collections.IEnumerator;
import system.collections.IList;

import java.io.*;
import java.util.List;
import java.util.Vector;

@Register(id = "csharp-roslyn", accept = "\\.cs" )
public class CSharpTreeGenerator extends TreeGenerator {
    public static String METAKEY_STARTLINE =  "startline";
    public static String METAKEY_ENDLINE =  "endline";

    private SyntaxTreeWrapper roslynTree;

    private static Object lockObject = new Object();
    private static boolean isInitialized;

    public CSharpTreeGenerator() throws IOException {
        initJni();
    }

    @Override
    protected TreeContext generate(Reader r) throws IOException {
        String input = getText(r);

        roslynwrapper.CShparSyntaxTreeGenerator generator = new roslynwrapper.CShparSyntaxTreeGenerator();

        // get tree from .NET DLL
        roslynTree = generator.GenerateTree(input);

        // convert to gumtree AST
        TreeContext tree = toTree(new TreeContext(), roslynTree.GetRoot(), null);

        // dispose in .NET DLL
        roslynTree.Dispose();

        return tree;
    }

    public void disposeNode(SyntaxNodeWrapper syntaxNodeWrapper) {
        if (syntaxNodeWrapper.getChildren().length == 0)
            Bridge.DisposeClrHandle(syntaxNodeWrapper.getClrHandle());
        else {
            for(int i = 0; i < syntaxNodeWrapper.getChildren().length; i++) {
                disposeNode(syntaxNodeWrapper.getChildren()[i]);
            }
        }
    }

    private TreeContext toTree(TreeContext ctx, SyntaxNodeWrapper node, ITree parent) {
        int type = node.getKind();
        ITree currentTreeNode = ctx.createTree(type, node.getLabel(), node.getKindName());

        // set line info
        currentTreeNode.setMetadata(METAKEY_STARTLINE, node.getStartLineNumber());
        currentTreeNode.setMetadata(METAKEY_ENDLINE, node.getEndLineNumber());

        // set span info
        currentTreeNode.setPos(node.getPosition());
        currentTreeNode.setLength(node.getLength());

        if (parent == null)
            ctx.setRoot(currentTreeNode);
        else
            currentTreeNode.setParentAndUpdateChildren(parent);

        // add modifiers if necessary
        addModifiers(ctx, currentTreeNode, node);
        // insert identifier node if necessary
        addIdentifier(ctx, currentTreeNode, node);

        for(SyntaxNodeWrapper child : node.getChildren()) {
            toTree(ctx, child, currentTreeNode);
        }

        return ctx;
    }

    private void addIdentifier(TreeContext ctx, ITree parent, SyntaxNodeWrapper node) {
        SyntaxTokenWrapper identifier = node.getIdentifier();

        if (identifier != null) {
            ITree identifierNode = ctx.createTree(identifier.getKind(), identifier.getLabel(), identifier.getKindName());

            // set line info
            identifierNode.setMetadata(METAKEY_STARTLINE, identifier.getLineNumber());
            identifierNode.setMetadata(METAKEY_ENDLINE, identifier.getLineNumber());

            // set span info
            identifierNode.setPos(identifier.getPosition());
            identifierNode.setLength(identifier.getLength());

            //TODO: check if parent is set correctly
            identifierNode.setParentAndUpdateChildren(parent);
        }
    }

    private void addModifiers(TreeContext ctx, ITree parent, SyntaxNodeWrapper node) {
        SyntaxTokenWrapper[] modifiers = node.getModifiersList();

        if (modifiers != null && modifiers.length > 0) {
            for (SyntaxTokenWrapper token : modifiers) {
                ITree modifierNode = ctx.createTree(token.getKind(), token.getLabel(), token.getKindName());

                // set line info
                modifierNode.setMetadata(METAKEY_STARTLINE, token.getLineNumber());
                modifierNode.setMetadata(METAKEY_ENDLINE, token.getLineNumber());

                // set span info
                modifierNode.setPos(token.getPosition());
                modifierNode.setLength(token.getLength());

                modifierNode.setParent(parent);
                parent.getChildren().add(modifierNode);
            }
        }
    }

    private String getText(Reader reader) throws IOException {
        if (reader == null)
            return "";

        char[] arr = new char[8 * 1024];
        StringBuilder buffer = new StringBuilder();
        int numCharsRead;
        while ((numCharsRead = reader.read(arr, 0, arr.length)) != -1) {
            buffer.append(arr, 0, numCharsRead);
        }
        return buffer.toString();
    }

    private static void initJni() throws IOException {
        if (!isInitialized) {
            synchronized (lockObject) {
                if (!isInitialized) {
                    try {
                        String currentWorkingDir = System.getProperty("user.dir");

                        String dllDirectory = currentWorkingDir + "/lib";
                        //System.out.println("Trying to find DLL at: " + dllDirectory);

                        // lib folder is subfolder of working dir
                        File dll = new File(dllDirectory + "/RoslynWrapper.j4n.dll");

                        if (!dll.exists()) {
                            // lib folder is on same level as working dir
                            dllDirectory = currentWorkingDir + "/../lib";
                            //System.out.println("Trying to find DLL at: " + dllDirectory);

                            dll = new File(dllDirectory + "/RoslynWrapper.j4n.dll");

                            if (!dll.exists())
                                throw new Exception("RoslynWrapper dll not found");
                        }

                        Bridge.init(new File(dllDirectory));
                        Bridge.LoadAndRegisterAssemblyFrom(dll);

                        isInitialized = true;
                    } catch (Exception e) {
                        throw new IOException("JNI could not be initialized", e);
                    }
                }
            }
        }
    }
}
