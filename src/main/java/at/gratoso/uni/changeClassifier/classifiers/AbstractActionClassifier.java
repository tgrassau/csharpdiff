package at.gratoso.uni.changeClassifier.classifiers;

import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.ChangeType;
import com.github.gumtreediff.actions.model.Action;
import com.github.gumtreediff.tree.ITree;

import java.util.Collection;
import java.util.HashSet;
import java.util.Vector;

public abstract class AbstractActionClassifier<T extends Action> {
    private boolean includeUnclassified = false;
    private final Collection<T> actions;

    protected HashSet<ITree> processedNodes;
    protected Collection<T> getActions() { return actions; }

    protected AbstractActionClassifier(Collection<T> actions, HashSet<ITree> processedNodes, boolean includeUnclassified) {
        this.actions = actions;
        this.processedNodes = processedNodes;
        this.includeUnclassified = includeUnclassified;
    }

    public Collection<SourceCodeChange> classify() {
        Collection<SourceCodeChange> result = new Vector<>();
        for (T action : actions) {
            if (!ignoreAction(action)) {
                SourceCodeChange change = classify(action);

                if (change == null)
                    continue;

                if (includeUnclassified
                        || change.getChangeType() != ChangeType.UNCLASSIFIED_CHANGE)
                    result.add(change);

                if (change.getChangeType() != ChangeType.UNCLASSIFIED_CHANGE)
                    processedNodes.add(change.getNode());
            }
        }
        return result;
    }

    protected abstract SourceCodeChange classify(T action);

    protected abstract boolean ignoreAction(T action);
}
