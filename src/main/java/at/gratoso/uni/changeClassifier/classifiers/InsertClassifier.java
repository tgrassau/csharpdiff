package at.gratoso.uni.changeClassifier.classifiers;

import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.changeClassifier.ChangeType;
import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.actions.model.Delete;
import com.github.gumtreediff.actions.model.Insert;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.tree.ITree;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class InsertClassifier extends AbstractActionClassifier<Insert> {
    private final MappingStore mappings;
    private List<Delete> deletes;

    public InsertClassifier(List<Insert> inserts, HashSet<ITree> processedNodes, boolean includeUnclassified, List<Delete> deletes, MappingStore mappings) {
        super(inserts, processedNodes, includeUnclassified);
        this.deletes = deletes;
        this.mappings = mappings;
    }

    @Override
    protected SourceCodeChange classify(Insert insert) {
        ITree node = insert.getNode();

        SourceCodeChange change = SourceCodeChange.Create(insert, mappings);

        // ignore namesapce names
        if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                    SyntaxKind.IdentifierName,
                    SyntaxKind.QualifiedName,
                    SyntaxKind.IdentifierToken}) // due to bug in mapper
                && SyntaxKindHelper.isOfKind(change.getRootNode(), SyntaxKind.NamespaceDeclaration))
            return null;

        if (SyntaxKindHelper.isAccessModifier(node)) {
            extractModifiersChange(change);
        } else if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.OperatorDeclaration,
                SyntaxKind.ConversionOperatorDeclaration})) {
            change.setChangeType(ChangeType.OPERATOR_INSERT);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.Attribute)) {
            change.setChangeType(ChangeType.ATTRIBUTE_INSERT);
        } else if ( SyntaxKindHelper.isOfKind(node, SyntaxKind.UsingDirective)) {
            change.setChangeType(ChangeType.USING_INSERT);
        } else if ( SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.MethodDeclaration,
                SyntaxKind.ConstructorDeclaration,
                SyntaxKind.SetAccessorDeclaration})) {
            change.setChangeType(ChangeType.ADDITIONAL_FUNCTIONALITY);
        } else if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.FieldDeclaration,
                SyntaxKind.PropertyDeclaration })) {
            change.setChangeType(ChangeType.ADDITIONAL_OBJECT_STATE);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ClassDeclaration)) {
            change.setChangeType(ChangeType.ADDITIONAL_CLASS);
        } else if (SyntaxKindHelper.isOfKind(change.getRootNode(), new SyntaxKind[] {
                    SyntaxKind.MethodDeclaration,
                    SyntaxKind.ConstructorDeclaration})) {
            handleSignatureChange(change);
        } else if (SyntaxKindHelper.isOfKind(change.getRootNode(), SyntaxKind.FieldDeclaration)) {
            handleFieldDeclarationChange(change);
        } else if (SyntaxKindHelper.isOfKind(change.getRootNode(), SyntaxKind.ClassDeclaration)) {
            handleClassDelcarationChange(change);
        }

        // TODO move to globally valid classifications to abstract class
        if (change.isUnclassified()) {
            ITree tmp = ITreeNodeHelper.getFirstAncestorOfKind(node, SyntaxKind.Attribute);
            if (tmp != null)
            {
                this.processedNodes.add(tmp);
                change.setChangeType(ChangeType.ATTRIBUTE_UPDATE);
            }
        }

        if (change.isUnclassified()) {
            handleNormalInsert(change);
        }

        return change;
    }

    @Override
    protected boolean ignoreAction(Insert insert) {
        ITree node = insert.getNode();

        // never ignore class inserts
        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ClassDeclaration))
            return false;

        if (SyntaxKindHelper.isOfKind(node,
                new SyntaxKind[] {
                        SyntaxKind.Block,
                        SyntaxKind.NamespaceDeclaration,
                        SyntaxKind.ClassDeclaration,
                        SyntaxKind.AttributeList
                }))
            return true;

        if (processedNodes.contains(node))
            return true;

        // check if parent has already been processed
        if (!Collections.disjoint(processedNodes, node.getParents()))
            return true;

        return false;
    }

    private void handleFieldDeclarationChange(SourceCodeChange change) {
        ITree node = change.getNode();
        ITree parent = node.getParent();

        if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.VariableDeclaration)
                && SyntaxKindHelper.isOfKind(node, new SyntaxKind[]
                    { SyntaxKind.PredefinedType, SyntaxKind.IdentifierName }))
        {
            ITree src = mappings.getSrc(parent);

            if (src != null                                                             // if mapping of parent can be found in src
                    && SyntaxKindHelper.isOfKind(src, SyntaxKind.VariableDeclaration)   // AND it is also a variable declaration
                    && SyntaxKindHelper.isOfKind(ITreeNodeHelper.getRootNode(src), SyntaxKind.FieldDeclaration)
                    )
            {
                change.setChangeType(ChangeType.ATTRIBUTE_TYPE_CHANGE);

                // remove delete action
                Delete delete = findDeleteOperation(parent, SyntaxKind.PredefinedType.getValue());
                if (delete == null)
                    delete = findDeleteOperation(parent, SyntaxKind.IdentifierName.getValue());

                deletes.remove(delete);
            }
        }
    }

    private void handleClassDelcarationChange(SourceCodeChange change) {
        ITree node = change.getNode();

        if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                    SyntaxKind.BaseList,        // first base type inserted
                    SyntaxKind.SimpleBaseType   // additional base type inserted
                })) {
            change.setChangeType(ChangeType.PARENT_TYPE_INSERT);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.TypeParameter)) {
            // TODO: introduce change type?? is it relevant?
        }
    }

    private void extractModifiersChange(SourceCodeChange change) {
        ITree node = change.getNode();
        ITree root = change.getRootNode();

        // check if parent was inserted
        if (mappings.getSrc(root) == null)
            return;

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.SealedKeyword)) {
            handleSealedChange(change);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.PublicKeyword)) {
            extractIncreasingAccessibilityChange(change);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.PrivateKeyword)) {
            extractDecreasingAccessibilityChange(change);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.InternalKeyword)) {
            Delete delPublic = findDeleteOperation(node.getParent(), SyntaxKind.PublicKeyword.getValue());
            Delete delProtected = findDeleteOperation(node.getParent(), SyntaxKind.PublicKeyword.getValue());
            Delete delPrivate = findDeleteOperation(node.getParent(), SyntaxKind.PrivateKeyword.getValue());

            if (delPublic != null) {
                // public -> internal
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delPublic);
            } else if (delPrivate != null) {
                // private -> internal
                change.setChangeType(ChangeType.INCREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delPrivate);
            } else  if (delProtected != null) {
                // protected -> internal
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delProtected);
            } else {
                // protected -> protected internal (|| internal protected)
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
            }

        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ProtectedKeyword)) {

            Delete delPublic = findDeleteOperation(node.getParent(), SyntaxKind.PublicKeyword.getValue());
            Delete delInternal = findDeleteOperation(node.getParent(), SyntaxKind.PublicKeyword.getValue());
            Delete delPrivate = findDeleteOperation(node.getParent(), SyntaxKind.PrivateKeyword.getValue());

            if (delPublic != null) {
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delPublic);
            } else if (delInternal != null)  {
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delInternal);
            } else if (delPrivate != null) {
                change.setChangeType(ChangeType.INCREASING_ACCESSIBILITY_CHANGE);
                deletes.remove(delPrivate);
            }

        }
    }

    private void handleSealedChange(SourceCodeChange change) {
        ITree root = change.getRootNode();

        if (SyntaxKindHelper.isOfKind(root, SyntaxKind.ClassDeclaration)) {
            change.setChangeType(ChangeType.REMOVING_CLASS_DERIVABILITY);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.MethodDeclaration)) {
            change.setChangeType(ChangeType.REMOVING_METHOD_OVERRIDABILITY);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.FieldDeclaration)) {
            change.setChangeType(ChangeType.REMOVING_ATTRIBUTE_MODIFIABILITY);
        }
    }

    private void extractIncreasingAccessibilityChange(SourceCodeChange change) {
        ITree node = change.getNode();

        Delete delInternal = findDeleteOperation(node.getParent(), SyntaxKind.InternalKeyword.getValue());
        Delete delProtected = findDeleteOperation(node.getParent(), SyntaxKind.ProtectedKeyword.getValue());
        Delete delPrivate = findDeleteOperation(node.getParent(), SyntaxKind.PrivateKeyword.getValue());

        if (delInternal != null)
            deletes.remove(delInternal);
        if (delProtected != null) {
            deletes.remove(delProtected);
        } else if (delPrivate != null)
            deletes.remove(delPrivate);

        change.setChangeType(ChangeType.INCREASING_ACCESSIBILITY_CHANGE);
    }

    private void extractDecreasingAccessibilityChange(SourceCodeChange change) {
        ITree node = change.getNode();

        Delete delProtected = findDeleteOperation(node.getParent(), SyntaxKind.ProtectedKeyword.getValue());
        Delete delPublic = findDeleteOperation(node.getParent(), SyntaxKind.PublicKeyword.getValue());

        if (delProtected != null)
            deletes.remove(delProtected);
        else if (delPublic != null)
            deletes.remove(delPublic);

        change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
    }

    private void handleSignatureChange(SourceCodeChange change) {
        if (SyntaxKindHelper.isOfKind(change.getRootNode(), new SyntaxKind[] {
                SyntaxKind.MethodDeclaration, SyntaxKind.ConstructorDeclaration})) {

            ITree node = change.getNode();
            ITree parent = node.getParent();

            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.Parameter)) {

                ITree movedParameter = ITreeNodeHelper.getMovedParameter(node, false, mappings);

                if (movedParameter == null)
                    change.setChangeType(ChangeType.PARAMETER_INSERT);
                else {
                    change.setChangeType(ChangeType.PARAMETER_ORDERING_CHANGE);

                    // any other change must be ignored
                    processedNodes.add(movedParameter);
                }
            } else if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.Parameter)
                    && !SyntaxKindHelper.isOfKind(node, SyntaxKind.IdentifierToken)) {
                // sub node of parameter is either name or type info
                change.setChangeType(ChangeType.PARAMETER_TYPE_CHANGE);
            }
        }
    }

    private void handleNormalInsert(SourceCodeChange change) {
        ITree node = change.getNode();

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ElseClause)) {
            change.setChangeType(ChangeType.ALTERNATIVE_PART_INSERT);
        } else if (ITreeNodeHelper.isPartOfConditionExpression(node)) {
            processedNodes.add(ITreeNodeHelper.getConditionExpressionRoot(node));
            change.setChangeType(ChangeType.CONDITION_EXPRESSION_CHANGE);
        } else if (SyntaxKindHelper.isStatement(node)) {
            change.setChangeType(ChangeType.STATEMENT_INSERT);
        } else {
            for(ITree parent : node.getParents()) {
                if (SyntaxKindHelper.isStatement(parent)) {
                    // add parent to processed, as change is due to nesting in statement
                    processedNodes.add(parent);

                    change.setChangeType(ChangeType.STATEMENT_UPDATE);
                    break;
                }
            }
        }
    }

    private Delete findDeleteOperation(ITree dstParent, int kind) {
        ITree srcParent = this.mappings.getSrc(dstParent);

        for(Delete del : deletes) {
            ITree node = del.getNode();
            if (node.getType() == kind
                    && node.getParent() == srcParent)
                return del;
        }

        return null;
    }
}
