package at.gratoso.uni.changeClassifier.classifiers;


import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.changeClassifier.ChangeType;
import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.actions.model.Delete;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.tree.ITree;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class DeleteClassifier extends AbstractActionClassifier<Delete> {

    private final MappingStore mappings;

    public DeleteClassifier(List<Delete> deletes, HashSet<ITree> processedNodes, boolean includeUnclassified, MappingStore mappings) {
        super(deletes, processedNodes, includeUnclassified);
        this.mappings = mappings;
    }

    @Override
    protected SourceCodeChange classify(Delete delete) {
        ITree node = delete.getNode();
        SourceCodeChange change = SourceCodeChange.Create(delete, mappings);

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.UsingDirective)) {
            change.setChangeType(ChangeType.USING_DELETE);
        } else if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.OperatorDeclaration,
                SyntaxKind.ConversionOperatorDeclaration})) {
            change.setChangeType(ChangeType.OPERATOR_DELETE);
        } else if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.Attribute,
                SyntaxKind.AttributeList })) {
            change.setChangeType(ChangeType.ATTRIBUTE_DELETE);
        } else if(SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.MethodDeclaration,
                SyntaxKind.ConstructorDeclaration,
                SyntaxKind.SetAccessorDeclaration})) {
            change.setChangeType(ChangeType.REMOVED_FUNCTIONALITY);
        } else if(SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.FieldDeclaration,
                SyntaxKind.PropertyDeclaration})) {
            change.setChangeType(ChangeType.REMOVED_OBJECT_STATE);
        } else if(SyntaxKindHelper.isOfKind(node, SyntaxKind.ClassDeclaration)) {
            change.setChangeType(ChangeType.REMOVED_CLASS);
        } else  {
            if (SyntaxKindHelper.isOfKind(change.getRootNode(), new SyntaxKind[] {
                    SyntaxKind.MethodDeclaration,
                    SyntaxKind.ConstructorDeclaration,
                    SyntaxKind.PropertyDeclaration})) {

                if (SyntaxKindHelper.isAccessModifier(node)) {
                    extractModifierChange(change);
                } else {
                    handleMethodSignatureChange(change);
                }
            } else if (SyntaxKindHelper.isOfKind(change.getRootNode(), SyntaxKind.ClassDeclaration)) {
                handleInheritanceChange(change);
            }
        }

        // TODO move to globally valid classifications to abstract class
        if (change.isUnclassified()) {
            ITree tmp = ITreeNodeHelper.getFirstAncestorOfKind(node, SyntaxKind.Attribute);
            if (tmp != null)
            {
                this.processedNodes.add(tmp);
                change.setChangeType(ChangeType.ATTRIBUTE_UPDATE);
            }
        }

        if (change.isUnclassified()) {
            handleNormalDelete(change);
        }

        return change;
    }

    @Override
    protected boolean ignoreAction(Delete action) {
        ITree node = action.getNode();

        // never ignore class deletes
        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ClassDeclaration))
            return false;

        // check if node was already processed
        if (processedNodes.contains(node))
            return true;

        List<ITree> parents = node.getParents();

        // check if any parent has already been processed
        if (!Collections.disjoint(processedNodes, parents))
            return true;

        ITree remainingAncestor = getFirstRemainingAncestor(node);

        // check if first remaining ancestor was processed
        if (processedNodes.contains(remainingAncestor))
            return true;

        // check if any remaining ancestor was processed
        if (!Collections.disjoint(processedNodes, remainingAncestor.getParents()))
            return true;

        // check if direct parent is also deleted (only process del root)
        return mappings.getDst(node.getParent()) == null;
    }

    private ITree getFirstRemainingAncestor(ITree node) {
        if (node.getParent() == null)
            return null;

        ITree parent = node.getParent();

        if (mappings.getDst(parent) != null)
            // parent has mapping in dst -> it cannot be deleted
            return mappings.getDst(parent);

        return getFirstRemainingAncestor(parent);
    }

    private void extractModifierChange(SourceCodeChange change) {

        ITree node = change.getNode();

        if (SyntaxKindHelper.isTopLevelType(node)
                && change.getRootNode() == null) {
            /* If no modifier is defined, top level types are internal
                Only internal and public are allowed */

            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.SealedKeyword)) {
                handleSealedDelete(change);
            } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.PublicKeyword)) {
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
            }
        } else {
            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.SealedKeyword)) {
                handleSealedDelete(change);
            } else if (!SyntaxKindHelper.isOfKind(node, SyntaxKind.PrivateKeyword))
                /* Any removal of access modifiers, that are not equal to "private",
                    result in a decreased accessibility */
                change.setChangeType(ChangeType.DECREASING_ACCESSIBILITY_CHANGE);
        }
    }

    private void handleSealedDelete(SourceCodeChange change) {
        ITree root = change.getRootNode();

        if (SyntaxKindHelper.isOfKind(root, SyntaxKind.ClassDeclaration)) {
            change.setChangeType(ChangeType.ADDING_CLASS_DERIVABILITY);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.MethodDeclaration)) {
            change.setChangeType(ChangeType.ADDING_METHOD_OVERRIDABILITY);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.FieldDeclaration)) {
            change.setChangeType(ChangeType.ADDING_ATTRIBUTE_MODIFIABILITY);
        }
    }

    private void handleMethodSignatureChange(SourceCodeChange change) {
        if (SyntaxKindHelper.isOfKind(change.getRootNode(), new SyntaxKind[] {
                SyntaxKind.MethodDeclaration, SyntaxKind.ConstructorDeclaration})) {

            ITree node = change.getNode();
            ITree parent = node.getParent();

            if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.ParameterList)) {
                // parameter removal
                change.setChangeType(ChangeType.PARAMETER_DELETE);
            } else if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.MethodDeclaration)) {
                // access modifiers are processed somewhere else

                if (!SyntaxKindHelper.isOfKind(
                        node,
                        new SyntaxKind[] {
                                SyntaxKind.TypeParameterList,
                                SyntaxKind.TypeParameterConstraintClause }))
                {
                    // ... this would be a return statement change ... should not be a delete action
                    // TODO: check if this block is never reached
                }
            }
        }
    }

    private void handleNormalDelete(SourceCodeChange change) {
        ITree node = change.getNode();

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ElseClause)) {
            change.setChangeType(ChangeType.ALTERNATIVE_PART_DELETE);
        } else if (SyntaxKindHelper.isStatement(node)) {
            change.setChangeType(ChangeType.STATEMENT_DELETE);
        } else if (ITreeNodeHelper.isPartOfConditionExpression(node)) {
            change.setChangeType(ChangeType.CONDITION_EXPRESSION_CHANGE);
            processedNodes.add(ITreeNodeHelper.getConditionExpressionRoot(node));
        } else {
            // check if is part of statement
            for(ITree parent : node.getParents()) {
                if (SyntaxKindHelper.isStatement(parent)) {
                    // add parent to processed, as change is due to nesting in statement
                    processedNodes.add(parent);

                    change.setChangeType(ChangeType.STATEMENT_UPDATE);
                    break;
                }
            }
        }
    }

    private void handleInheritanceChange(SourceCodeChange change) {
        ITree node = change.getNode();

        if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                    SyntaxKind.BaseList,        // last base type removed
                    SyntaxKind.SimpleBaseType   // base type removed
            })) {
            change.setChangeType(ChangeType.PARENT_TYPE_DELETE);
        }
    }
}
