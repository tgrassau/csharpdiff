package at.gratoso.uni.changeClassifier.classifiers;


import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.changeClassifier.ChangeType;
import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.actions.model.Move;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.tree.ITree;

import java.util.*;

public class MoveClassifier extends AbstractActionClassifier<Move> {
    private final MappingStore mappings;

    public MoveClassifier(List<Move> moves, HashSet<ITree> processedNodes, boolean includeUnclassified, MappingStore mappings) {
        super(moves, processedNodes, includeUnclassified);
        this.mappings = mappings;
    }

    @Override
    protected SourceCodeChange classify(Move move) {
        ITree node = move.getNode();
        ITree root = ITreeNodeHelper.getRootNode(node);

        SourceCodeChange change = SourceCodeChange.Create(move, mappings);

        if (SyntaxKindHelper.isOfKind(root, new SyntaxKind[] {
                SyntaxKind.MethodDeclaration,
                SyntaxKind.ConstructorDeclaration,
                SyntaxKind.PropertyDeclaration})) {

            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.Parameter)) {
                // parameter move
                change.setChangeType(ChangeType.PARAMETER_ORDERING_CHANGE);
            } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.Block)) {
                change.setChangeType(ChangeType.STATEMENT_PARENT_CHANGE);
            } else {
                if (ITreeNodeHelper.isPartOfConditionExpression(node)) {
                    // todo check if this needs to be covered
                    change.setChangeType(ChangeType.CONDITION_EXPRESSION_CHANGE);
                    processedNodes.add(ITreeNodeHelper.getConditionExpressionRoot(node));
                } else if (SyntaxKindHelper.isStatement(node)) {
                    ITree dstNode = mappings.getDst(node);
                    if (node.getParent() == mappings.getSrc(dstNode.getParent())) {
                        change.setChangeType(ChangeType.STATEMENT_ORDERING_CHANGE);
                    } else {
                        // this should by a parent statement change but it does not work for large files
                        change.setChangeType(ChangeType.STATEMENT_PARENT_CHANGE);
                    }
                }
            }
        }

        return change;
    }

    @Override
    protected boolean ignoreAction(Move action) {
        // check if any ancestor has already been processed
        if (!Collections.disjoint(processedNodes, action.getNode().getParents()))
            return true;

        // check if any ancestor in dst has already been processed
        if (!Collections.disjoint(processedNodes,
                mappings.getDst(action.getNode()).getParents()))
            return true;

        return false;
    }
}
