package at.gratoso.uni.changeClassifier.classifiers;


import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.changeClassifier.ChangeType;
import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.actions.model.Update;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.tree.ITree;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class UpdateClassifier extends AbstractActionClassifier<Update> {
    private final MappingStore mappings;

    public UpdateClassifier(List<Update> updates, HashSet<ITree> processedNodes, boolean includeUnclassified, MappingStore mappings) {
        super(updates,processedNodes, includeUnclassified);
        this.mappings = mappings;
    }

    @Override
    protected SourceCodeChange classify(Update update) {
        SourceCodeChange change = SourceCodeChange.Create(update, mappings);

        ITree node = update.getNode();
        ITree parent = node.getParent();
        ITree root = ITreeNodeHelper.getRootNode(node);

        if (SyntaxKindHelper.isOfKind(root, SyntaxKind.ConstructorDeclaration)) {
            handleParamterChange(change);
        }
        if (SyntaxKindHelper.isOfKind(root, SyntaxKind.MethodDeclaration)) {
            if (root == parent && SyntaxKindHelper.isOfKind(node, SyntaxKind.IdentifierToken)) {
                change.setChangeType(ChangeType.METHOD_RENAMING);
            } else {
                handleMethodSignatureChange(change);
            }
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ClassDeclaration)) {
            change.setChangeType(ChangeType.CLASS_RENAMING);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.ClassDeclaration)) {
            handleInheritanceChange(change);
        } else if (SyntaxKindHelper.isOfKind(root, SyntaxKind.FieldDeclaration)) {
            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.IdentifierToken)) {
                change.setChangeType(ChangeType.ATTRIBUTE_RENAMING);
            }
            else if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.VariableDeclaration)
                    && SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                        SyntaxKind.PredefinedType, SyntaxKind.IdentifierName })) {
                change.setChangeType(ChangeType.ATTRIBUTE_TYPE_CHANGE);
            }
        }

        if (change.isUnclassified()) {
            handleNormalUpdate(change);
        }

        return change;
    }

    @Override
    protected boolean ignoreAction(Update action) {
        // check has to be performed in dst-nodes
        ITree dstNode = mappings.getDst(action.getNode());

        // check if original node has already been processed
        if (processedNodes.contains(action.getNode()))
            return true;

        // check if new node has already been processed
        if (processedNodes.contains(dstNode))
            return true;

        // check if any parent has already been processed
        if (!Collections.disjoint(processedNodes, dstNode.getParents()))
            return true;

        // check if any parent has already been processed
        if (!Collections.disjoint(processedNodes, action.getNode().getParents()))
            return true;

        return false;
    }

    private void handleMethodSignatureChange(SourceCodeChange change) {
        ITree node = change.getAction().getNode();
        ITree parent = node.getParent();

        if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.Parameter)) {
            handleParamterChange(change);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.TypeParameter)) {
            // TODO: type parameter changed
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.TypeParameterConstraintClause)) {
            // TODO: type parameter constraints changed
        } else if (SyntaxKindHelper.isOfKind(parent, SyntaxKind.MethodDeclaration)
                && !SyntaxKindHelper.isOfKind(node, SyntaxKind.Block)) {
            // only return statement left

            String oldLabel = node.getLabel();
            String newLabel = ((Update)change.getAction()).getValue();

            if (oldLabel == "void") {
                // void -> *
                change.setChangeType(ChangeType.RETURN_TYPE_INSERT);
            } else {
                if (newLabel == "void") {
                    // * -> void
                    change.setChangeType(ChangeType.RETURN_TYPE_DELETE);
                } else {
                    // * -> *
                    change.setChangeType(ChangeType.RETURN_TYPE_CHANGE);
                }
            }
        }
    }

    private void handleParamterChange(SourceCodeChange change) {
        ITree node = change.getAction().getNode();
        ITree param = node.getParent();

        if (ITreeNodeHelper.getFirstAncestorOfKind(node, SyntaxKind.ParameterList) == null)
            return;

        ITree movedParam = ITreeNodeHelper.getMovedParameter(param, true, mappings);

        if (movedParam == null) {
            if (SyntaxKindHelper.isOfKind(node, SyntaxKind.IdentifierToken))
                change.setChangeType(ChangeType.PARAMETER_RENAMING);
            else
                change.setChangeType(ChangeType.PARAMETER_TYPE_CHANGE);
        } else {
            change.setChangeType(ChangeType.PARAMETER_ORDERING_CHANGE);

            // mark old and new param as processed
            processedNodes.add(param);
            processedNodes.add(movedParam);
        }
    }

    private void handleNormalUpdate(SourceCodeChange change) {
        ITree node = change.getNode();

        if (ITreeNodeHelper.isPartOfConditionExpression(node)) {
            change.setChangeType(ChangeType.CONDITION_EXPRESSION_CHANGE);
            processedNodes.add(ITreeNodeHelper.getConditionExpressionRoot(node));
        } else if (SyntaxKindHelper.isStatement(node)) {
            change.setChangeType(ChangeType.STATEMENT_UPDATE);
        } else {
            for(ITree parent : node.getParents()) {
                if (SyntaxKindHelper.isStatement(parent)) {
                    // add parent to processed, as change is due to nesting in statement
                    processedNodes.add(mappings.getSrc(parent));
                    processedNodes.add(parent);

                    change.setChangeType(ChangeType.STATEMENT_UPDATE);
                    break;
                }
            }
        }
    }

    private void handleInheritanceChange(SourceCodeChange change) {
        ITree node = change.getNode();

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.SimpleBaseType)) {
            change.setChangeType(ChangeType.PARENT_TYPE_CHANGE);
        }
    }
}
