package at.gratoso.uni.changeClassifier;

import at.gratoso.uni.changeClassifier.classifiers.*;
import com.github.gumtreediff.actions.ActionGenerator;
import com.github.gumtreediff.actions.model.*;
import com.github.gumtreediff.matchers.CompositeMatchers;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.tree.ITree;
import com.github.gumtreediff.tree.TreeContext;

import java.util.HashSet;
import java.util.List;
import java.util.Vector;

public class CSharpChangeClassifier {
    private final boolean includeUnclassified;

    public CSharpChangeClassifier(boolean includeUnclassified) {
        this.includeUnclassified = includeUnclassified;
    }

    public CSharpChangeClassifier() {
        this(false);
    }

    public List<SourceCodeChange> classify(TreeContext src, TreeContext dst) {
        Matcher m = new CompositeMatchers.ClassicGumtree(src.getRoot(), dst.getRoot(), new MappingStore());
        m.match();

        MappingStore mappings = m.getMappings();

        ActionGenerator g = new ActionGenerator(src.getRoot(), dst.getRoot(), mappings);
        g.generate();
        List<Action> actions = g.getActions();

        return classify(actions, mappings);
    }

    public List<SourceCodeChange> classify(List<Action> actions, MappingStore mappings) {

        List<SourceCodeChange> result = new Vector<>();

        List<Insert> inserts = new Vector<>();
        List<Update> updates = new Vector<>();
        List<Delete> deletes = new Vector<>();
        List<Move> moves = new Vector<>();

        for(Action a : actions) {
            if (a instanceof Insert)
                inserts.add((Insert)a);
            else if (a instanceof Update)
                updates.add((Update) a);
            else if (a instanceof Delete)
                deletes.add((Delete) a);
            else if (a instanceof Move)
                moves.add((Move)a);
        }

        HashSet<ITree> processedNodes = new HashSet<>();

        AbstractActionClassifier insClassifier
                = new InsertClassifier(inserts, processedNodes, includeUnclassified, deletes, mappings);
        AbstractActionClassifier updClassifier
                = new UpdateClassifier(updates, processedNodes, includeUnclassified, mappings);
        AbstractActionClassifier delClassifier
                = new DeleteClassifier(deletes, processedNodes, includeUnclassified, mappings);
        AbstractActionClassifier movClassifier
                = new MoveClassifier(moves, processedNodes, includeUnclassified, mappings);

        result.addAll(insClassifier.classify());
        result.addAll(updClassifier.classify());
        result.addAll(delClassifier.classify());
        result.addAll(movClassifier.classify());

        return result;
    }
}
