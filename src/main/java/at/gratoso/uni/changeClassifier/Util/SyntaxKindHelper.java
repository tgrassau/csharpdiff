package at.gratoso.uni.changeClassifier.Util;


import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.tree.ITree;

public class SyntaxKindHelper {
    public static boolean isAccessModifier(ITree node) {
        SyntaxKind[] modifiers = new SyntaxKind[] {
                SyntaxKind.PublicKeyword,
                SyntaxKind.InternalKeyword,
                SyntaxKind.ProtectedKeyword,
                SyntaxKind.PrivateKeyword,
                SyntaxKind.SealedKeyword
        };

        return isOfKind(node, modifiers);
    }

    public static boolean isTopLevelType(ITree node) {
        SyntaxKind[] structureNodeTypes = new SyntaxKind[] {
                SyntaxKind.ClassDeclaration,
                SyntaxKind.InterfaceDeclaration,
                SyntaxKind.EnumDeclaration,
                SyntaxKind.StructDeclaration
        };

        return isOfKind(node, structureNodeTypes);
    }

    public static boolean isStructureNode(ITree node) {
        SyntaxKind[] structureNodeTypes = new SyntaxKind[] {
                SyntaxKind.CompilationUnit,
                SyntaxKind.NamespaceDeclaration,
                SyntaxKind.ClassDeclaration,
                SyntaxKind.InterfaceDeclaration,
                SyntaxKind.EnumDeclaration,
                SyntaxKind.ConstructorDeclaration,
                SyntaxKind.MethodDeclaration,
                SyntaxKind.PropertyDeclaration,
                SyntaxKind.FieldDeclaration,
                SyntaxKind.EventDeclaration,
                SyntaxKind.StructDeclaration
        };

        return isOfKind(node, structureNodeTypes);
    }

    public static boolean isOfKind(ITree node, SyntaxKind kind) {
        return isOfKind(node, new SyntaxKind[] {kind});
    }

    public static boolean isOfKind(ITree node, SyntaxKind[] kinds) {
        if (node != null) {
            for(SyntaxKind kind : kinds)
                if (kind.getValue() == node.getType())
                    return true;
        }

        return false;
    }

    public static boolean hasIdentifierToken(ITree node) {
        SyntaxKind[] nodesWithIdentifierTokens = new SyntaxKind[] {
                SyntaxKind.ClassDeclaration,
                SyntaxKind.DelegateDeclaration,
                SyntaxKind.EventDeclaration,
                SyntaxKind.EnumDeclaration,
                SyntaxKind.InterfaceDeclaration,
                SyntaxKind.MethodDeclaration,
                SyntaxKind.PropertyDeclaration,
        };

        return isOfKind(node, nodesWithIdentifierTokens);
    }

    public static boolean hasConditionExpression(ITree node) {
        return SyntaxKindHelper.isOfKind(node, new SyntaxKind[] {
                SyntaxKind.IfStatement,
                SyntaxKind.ForStatement,
                SyntaxKind.WhileStatement,
                SyntaxKind.DoStatement});
    }

    public static boolean isStatement(ITree node) {
        SyntaxKind[] statementKinds = new SyntaxKind[] {
                SyntaxKind.ExpressionStatement,
                SyntaxKind.EmptyStatement,
                SyntaxKind.LabeledStatement,
                SyntaxKind.GotoStatement,
                SyntaxKind.GotoCaseStatement,
                SyntaxKind.GotoDefaultStatement,
                SyntaxKind.BreakStatement,
                SyntaxKind.ContinueStatement,
                SyntaxKind.ReturnStatement,
                SyntaxKind.YieldReturnStatement,
                SyntaxKind.YieldBreakStatement,
                SyntaxKind.ThrowStatement,
                SyntaxKind.WhileStatement,
                SyntaxKind.DoStatement,
                SyntaxKind.ForStatement,
                SyntaxKind.ForEachStatement,
                SyntaxKind.UsingStatement,
                SyntaxKind.FixedStatement,
                SyntaxKind.CheckedStatement,
                SyntaxKind.UncheckedStatement,
                SyntaxKind.UnsafeStatement,
                SyntaxKind.LockStatement,
                SyntaxKind.IfStatement,
                SyntaxKind.SwitchStatement,
                SyntaxKind.CaseSwitchLabel,
                SyntaxKind.DefaultSwitchLabel,
                SyntaxKind.TryStatement,
                SyntaxKind.GlobalStatement,
                SyntaxKind.CatchClause,
                SyntaxKind.FinallyClause,
                SyntaxKind.ParenthesizedLambdaExpression,
                SyntaxKind.SimpleLambdaExpression,
                SyntaxKind.InvocationExpression,
                SyntaxKind.LocalDeclarationStatement
        };

        return isOfKind(node, statementKinds);
    }

    public static SyntaxKind getByValue(int kindValue) {
        for(SyntaxKind kind : SyntaxKind.values())
            if (kind.getValue() == kindValue)
                return kind;

        return null;
    }
}
