package at.gratoso.uni.changeClassifier.Util;

import at.gratoso.uni.gen.csharp.SyntaxKind;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.matchers.Matcher;
import com.github.gumtreediff.tree.ITree;

import java.util.List;

public class ITreeNodeHelper {

    public static ITree getMovedParameter(ITree param, boolean isSource, MappingStore mappings) {
        if (!SyntaxKindHelper.isOfKind(param, SyntaxKind.Parameter))
            return null;

        // get parameter list
        ITree paramList = param.getParent();

        // get corresponding parameter list
        ITree otherParamList = isSource ? mappings.getDst(paramList) : mappings.getSrc(paramList);

        if (otherParamList != null) {
            for(ITree otherParam : otherParamList.getChildren()) {
                if (equals(param, otherParam)) {
                    return otherParam;
                }
            }
        }

        return null;
    }

    /**
     * Returns true if the trees are an exact match
     *
     * @param a
     * @param b
     * @return
     */
    private static boolean equals(ITree a, ITree b) {
        if (a.getType() != b.getType()
                || !a.getLabel().equals(b.getLabel()))
            return false;

        ITree aClone = a.deepCopy();
        ITree bClone = b.deepCopy();

        // we only want the subtree to be matched
        aClone.setParent(null);
        bClone.setParent(null);

        MappingStore ms = new MappingStore();
        Matcher m = new Matcher(aClone, bClone, ms) {
            @Override
            public void match() {
                List<ITree> srcs = this.getSrc().getDescendants();
                List<ITree> dsts = this.getDst().getDescendants();

                if (srcs.size() != dsts.size())
                    return;

                for(int i = 0; i < srcs.size(); i++) {
                    ITree s = srcs.get(i);
                    ITree d = dsts.get(i);

                    if (s.getType() == d.getType()
                            && s.getLabel().equals(d.getLabel()))
                        this.addMapping(s, d);
                }
            }
        };
        m.match();

        // tree have to be an excact match -> ever node has to have a mapping
        for (ITree subTree : aClone.getDescendants())
            if (ms.getDst(subTree) == null)
                return false;

        return true;
    }

    public static String getUniqueNodeName(ITree node) {
        String nodeName = getNodeName(node);

        if (!SyntaxKindHelper.isStructureNode(node))
            return nodeName;

        if (nodeName == null || nodeName.equals(""))
            return "";

        ITree currentRoot = getRootNode(node);
        while (currentRoot != null
                && !SyntaxKindHelper.isOfKind(currentRoot, SyntaxKind.CompilationUnit)) {
            nodeName = getNodeName(currentRoot) + "." + nodeName;
            currentRoot = getRootNode(currentRoot);
        }

        return nodeName;
    }

    public static ITree getRootNode(ITree node) {
        ITree parent = node.getParent();

        if (parent == null)
            return null;

        if (SyntaxKindHelper.isStructureNode(parent))
            return parent;
        else
            return getRootNode(parent);
    }

    public static ITree getTopLevelNode(ITree node) {
        if (SyntaxKindHelper.isTopLevelType(node))
            return node;

        ITree parent = node.getParent();

        if (parent == null)
            return null;

        if (SyntaxKindHelper.isTopLevelType(parent))
            return parent;
        else
            return getTopLevelNode(parent);
    }

    public static String getNodeName(ITree node) {
        // TODO support for Variable_Declaration

        if (node == null)
            return "";

        if (SyntaxKindHelper.isOfKind(node, SyntaxKind.NamespaceDeclaration)) {
            return getNamespaceName(node);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.FieldDeclaration)) {
            return getFieldName(node);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.MethodDeclaration)) {
            return getMethodName(node);
        } else if (SyntaxKindHelper.isOfKind(node, SyntaxKind.ConstructorDeclaration)) {
            return getConstructorName(node);
        } else if (SyntaxKindHelper.hasIdentifierToken(node)) {
            return getIdentifier(node);
        }

        return node.getLabel();
    }

    private static String getFieldName(ITree node) {
        if (!SyntaxKindHelper.isOfKind(node, SyntaxKind.FieldDeclaration))
            return "";

        for (ITree descendant : node.getDescendants())
            if (SyntaxKindHelper.isOfKind(descendant, SyntaxKind.IdentifierToken))
                return descendant.getLabel();

        return "";
    }

    private static String getNamespaceName(ITree node) {
        if (!SyntaxKindHelper.isOfKind(node, SyntaxKind.NamespaceDeclaration))
            return "";

        for(ITree child : node.getChildren()) {
            if (SyntaxKindHelper.isOfKind(child, SyntaxKind.IdentifierName))
                return child.getLabel();
            if (SyntaxKindHelper.isOfKind(child, SyntaxKind.QualifiedName)) {
                String retVal = "";
                for(ITree nameChild : child.getDescendants()) {
                    if (SyntaxKindHelper.isOfKind(nameChild, SyntaxKind.IdentifierName)) {
                        retVal += "." + nameChild.getLabel();
                    }
                }

                return retVal.substring(1);
            }
        }

        return "";
    }

    private static String getMethodName(ITree node) {
        if (!SyntaxKindHelper.isOfKind(node, SyntaxKind.MethodDeclaration))
            return "";

        String name = getIdentifier(node);
        String parameters = getParameterString(node);

        return String.format("%s(%s)", name, parameters);
    }

    private static String getConstructorName(ITree node) {
        if (!SyntaxKindHelper.isOfKind(node, SyntaxKind.ConstructorDeclaration))
            return "";

        ITree classNode = ITreeNodeHelper.getTopLevelNode(node);

        String name = ITreeNodeHelper.getNodeName(classNode);
        String parameters = getParameterString(node);

        return String.format("%s(%s)", name, parameters);
    }

    private static String getParameterString(ITree node) {
        ITree parameterList = getFirstChildOfKind(node, SyntaxKind.ParameterList);

        String parameters = "";

        if (parameterList != null && parameterList.getChildren().size() > 0) {
            for(ITree parameter : parameterList.getChildren()) {
                // TODO find better way to format parameters ... just a hack ... -> provide formatted type info Tree

                ITree parameterType = parameter.getChild(1);
                if (SyntaxKindHelper.isOfKind(parameterType, SyntaxKind.GenericName))
                    parameters += "," + parameterType.getLabel() + "<>";
                else if (SyntaxKindHelper.isOfKind(parameterType, SyntaxKind.ArrayType))
                    parameters += "," + parameterType.getChild(0).getLabel() + "[]";
                else
                    parameters += "," + getTypeName(parameterType);
            }

            parameters = parameters.substring(1);
        }

        return parameters;
    }

    private static String getTypeName(ITree node) {

        if (SyntaxKindHelper.isOfKind(node, new SyntaxKind[] { SyntaxKind.IdentifierName, SyntaxKind.PredefinedType }))
            return node.getLabel();

        String retVal = "";

        for(ITree child : node.getChildren()) {
            retVal = getTypeName(child);

            if(!retVal.equals(""))
                return retVal;
        }

        return retVal;
    }

    private static String getIdentifier(ITree node) {
        ITree idNode = getFirstChildOfKind(node, SyntaxKind.IdentifierToken);

        if (idNode != null)
            return idNode.getLabel();

        return "";
    }

    public static ITree getFirstChildOfKind(ITree node, SyntaxKind kind) {
        for(ITree child : node.getChildren())
            if (SyntaxKindHelper.isOfKind(child, kind))
                return child;

        return null;
    }

    public static ITree getFirstAncestorOfKind(ITree node, SyntaxKind kind) {
        for(ITree ancestor : node.getParents())
            if (SyntaxKindHelper.isOfKind(ancestor, kind))
                return ancestor;

        return null;
    }

    public static boolean isPartOfConditionExpression(ITree node) {
        return getConditionExpressionRoot(node) != null;
    }

    public static ITree getConditionExpressionRoot(ITree node) {
        ITree parent = node.getParent();

        if (parent == null
                || SyntaxKindHelper.isStructureNode(parent))
            return null;


        if (SyntaxKindHelper.hasConditionExpression(parent))
        {
            if (SyntaxKindHelper.isOfKind(parent, new SyntaxKind[] {
                    SyntaxKind.IfStatement, SyntaxKind.WhileStatement })) {
                // test if and while
                if (parent.getChild(0) == node)
                    return parent;
            } else {
                // test for and do
                if (parent.getChild(1) == node)
                    return parent;
            }
        }

        return getConditionExpressionRoot(parent);
    }
}
