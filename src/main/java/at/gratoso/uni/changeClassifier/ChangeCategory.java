package at.gratoso.uni.changeClassifier;

public enum ChangeCategory {
    ADDED_CLASS,
    REMOVED_CLASS,
    CLASS_DECLARATION,
    METHOD_DECLARATION,
    ATTRIBUTE_DECLARATION,
    BODY_STATEMENTS,
    BODY_CONDITIONS,
    USINGS,
    ATTRIBUTES,
    OTHER,
    UNCLASSIFIED
}
