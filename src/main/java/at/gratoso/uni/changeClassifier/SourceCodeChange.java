package at.gratoso.uni.changeClassifier;


import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import com.github.gumtreediff.actions.model.*;
import com.github.gumtreediff.matchers.MappingStore;
import com.github.gumtreediff.tree.ITree;

public class SourceCodeChange  {
    private ITree rootNode;
    private ITree topLevelNode;

    private Action action;
    private ChangeType changeType = ChangeType.UNCLASSIFIED_CHANGE;
    private ITree node;

    private SourceCodeChange(Action action, ITree node) {
        this.action = action;
        this.node = node;
    }

    public boolean isUnclassified() {
        return changeType == ChangeType.UNCLASSIFIED_CHANGE;
    }

    public Action getAction() {
        return this.action;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public String getActionType() {
        if (action instanceof Insert)
            return "INS";
        if (action instanceof Delete)
            return "DEL";
        if (action instanceof Update)
            return "UPD";
        if (action instanceof Move)
            return "MOV";

        return "";
    }

    public ITree getNode() {
        return this.node;
    }

    public ITree getRootNode() {
        if (rootNode == null)
            rootNode = ITreeNodeHelper.getRootNode(this.getNode());

        return rootNode;
    }

    public ITree getTopLevelNode() {
        if (topLevelNode == null)
            topLevelNode = ITreeNodeHelper.getTopLevelNode(this.getNode());

        return topLevelNode;
    }

    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }

    @Override
    public String toString() {
        // TODO: honor change type
        return this.action.toString();
    }

    public static SourceCodeChange Create(Action action, MappingStore mappings) {
        ITree node = action.getNode();
        if (action instanceof Update)
            node = mappings.getDst(node);

        return new SourceCodeChange(action, node);
    }
}
