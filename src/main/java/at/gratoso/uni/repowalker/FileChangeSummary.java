package at.gratoso.uni.repowalker;

import at.gratoso.uni.changeClassifier.SourceCodeChange;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;

import java.util.List;

public class FileChangeSummary {
    private final RevCommit commit;
    private final RevCommit parentCommit;
    private DiffEntry diff;
    private final List<SourceCodeChange> changes;
    private String testDir;

    public FileChangeSummary(RevCommit commit, RevCommit parentCommit, DiffEntry diff, List<SourceCodeChange> changes, String testDir) {
        this.commit = commit;
        this.parentCommit = parentCommit;
        this.diff = diff;
        this.changes = changes;
        this.testDir = testDir;
    }

    public RevCommit getCommit() {
        return commit;
    }

    public RevCommit getParentCommit() {
        return parentCommit;
    }

    public String getSourceFileName() {
        return diff.getOldPath().equals(DiffEntry.DEV_NULL)
                ? null : diff.getOldPath();
    }

    public String getDestinationFileName() {
        return diff.getNewPath().equals(DiffEntry.DEV_NULL)
                ? null : diff.getNewPath();
    }

    public List<SourceCodeChange> getChanges() {
        return changes;
    }

    public boolean isTestCode() {
        return this.getDestinationFileName() != null && this.getDestinationFileName().startsWith(this.testDir)
                || this.getSourceFileName() != null && this.getSourceFileName().startsWith(this.testDir);
    }
}
