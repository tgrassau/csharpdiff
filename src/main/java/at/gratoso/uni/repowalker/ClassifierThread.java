package at.gratoso.uni.repowalker;


import at.gratoso.uni.changeClassifier.CSharpChangeClassifier;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.gen.csharp.CSharpTreeGenerator;
import com.github.gumtreediff.tree.TreeContext;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.NullInputStream;
import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.AbbreviatedObjectId;
import org.eclipse.jgit.lib.AnyObjectId;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.filter.*;
import org.eclipse.jgit.util.io.NullOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

class ClassifierThread extends Thread {

    private final Repository repository;
    private final Collection<RevCommit> commits;
    private final Queue<FileChangeSummary> changeQueue;
    private String prodDir;
    private String testDir;
    private String[] dirsToExclude;

    public ClassifierThread(
            Repository repository,
            Collection<RevCommit> commits,
            Queue<FileChangeSummary> changeQueue) {
        this.repository = repository;
        this.commits = commits;
        this.changeQueue = changeQueue;
    }

    public ClassifierThread(Repository repository,
                            Collection<RevCommit> commits,
                            Queue<FileChangeSummary> changeQueue,
                            String prodDir,
                            String testDir,
                            String[] dirsToExclude) {
        this(repository, commits, changeQueue);
        this.prodDir = prodDir.endsWith("/") ? prodDir : prodDir + "/";
        this.testDir = testDir.endsWith("/") ? testDir : testDir + "/";
        this.dirsToExclude = dirsToExclude;
    }

    public void run() {
        for (RevCommit commit : commits) {
            try {
                analyzeCommit(commit);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GitAPIException e) {
                e.printStackTrace();
            }
        }
    }

    private void analyzeCommit(RevCommit commit) throws IOException, GitAPIException {
        StopWatch commitStop = new StopWatch();
        commitStop.start();

        System.out.println(String.format("\t %s", commit.getId()));

        TreeFilter filter = getTreeFilter();

        RevCommit[] parents = commit.getParents().length > 0
                ? commit.getParents()           // diff with parents
                : new RevCommit[] { null };     // diff with null

        long lastGCTime = 0;

        for (RevCommit parent : parents) {
            DiffFormatter df = new DiffFormatter(NullOutputStream.INSTANCE);
            df.setRepository(repository);
            df.setPathFilter(filter);
            df.setDetectRenames(true);

            List<DiffEntry> diffs = df.scan(parent, commit.getTree());

            long treeTime, changesTime;

            for (DiffEntry diff : diffs) {
                String destinationFileName =
                        diff.getNewPath().equals(DiffEntry.DEV_NULL)
                                ? null : diff.getNewPath();

                if (skipFile(destinationFileName))
                    continue;

                StopWatch stopwatch = new StopWatch();
                stopwatch.start();

                InputStream src = getFileStream(diff.getOldId());
                InputStream dst = getFileStream(diff.getNewId());

                stopwatch.split();

                CSharpTreeGenerator treeGenerator = new CSharpTreeGenerator();
                TreeContext srcContext = treeGenerator.generateFromStream(src);
                TreeContext dstContext = treeGenerator.generateFromStream(dst);

                stopwatch.split();
                treeTime = stopwatch.getSplitTime();

                CSharpChangeClassifier classifier = new CSharpChangeClassifier(true);

                List<SourceCodeChange> changes = classifier.classify(srcContext, dstContext);

                if (changes.size() > 0) {
                    FileChangeSummary changeSummary = new FileChangeSummary(
                            commit,
                            parent,
                            diff,
                            changes,
                            this.testDir);

                    changeQueue.add(changeSummary);
                }

                stopwatch.split();
                changesTime = stopwatch.getSplitTime() - treeTime;

                src.close();
                dst.close();

                stopwatch.split();

                System.out.println(String.format("\tt: %s\tc: %s\t%s", treeTime, changesTime, diff.getNewPath()));

                if (commitStop.getTime() - lastGCTime > 10000) {
                    commitStop.suspend();
                    lastGCTime = commitStop.getTime();
                    gc(2500);
                    commitStop.resume();
                }
            }
        }

        commitStop.stop();

        gc((commitStop.getTime() - lastGCTime) / 4);
    }

    private InputStream getFileStream(AbbreviatedObjectId objectId) throws IOException {
        AnyObjectId anyObjectId = objectId.toObjectId();

        if (anyObjectId != null && !anyObjectId.equals(ObjectId.zeroId()))
            return repository.open(objectId.toObjectId()).openStream();

        return new NullInputStream(0);
    }

    private boolean skipFile(String fileName) {
        if (fileName == null)
            return false;

        if (fileName.contains("/Properties/"))
            return true;

        if (dirsToExclude != null)
            for(String dir : this.dirsToExclude)
                if (fileName.startsWith(dir))
                    return true;

        return false;
    }

    private TreeFilter getTreeFilter() {

        Vector<TreeFilter> srcFilter = new Vector<>();
        srcFilter.add(PathFilter.create(this.prodDir));
        srcFilter.add(PathSuffixFilter.create(".cs"));

        Vector<TreeFilter> testFilter = new Vector<>();
        testFilter.add(PathFilter.create(this.testDir));
        testFilter.add(PathSuffixFilter.create(".cs"));

        return OrTreeFilter.create(new TreeFilter[]{
                AndTreeFilter.create(srcFilter),
                AndTreeFilter.create(testFilter) });
    }

    private void gc(long ms) {
        ms = Math.max(500, ms);

        try {
            while (ms > 0) {
                // tell the gc that it would be nice to free some memory ...
                Runtime.getRuntime().gc();
                // we sleep here to allow the Finalizer thread to get the possibility to work
                this.sleep(250);
                ms-=250;
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
