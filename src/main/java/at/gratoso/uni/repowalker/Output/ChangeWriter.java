package at.gratoso.uni.repowalker.Output;

import at.gratoso.uni.repowalker.FileChangeSummary;

import java.io.Closeable;
import java.io.IOException;

public interface ChangeWriter extends Closeable {
    void write(FileChangeSummary changeSummary) throws IOException;
}
