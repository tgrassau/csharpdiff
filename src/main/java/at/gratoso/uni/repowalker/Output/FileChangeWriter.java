package at.gratoso.uni.repowalker.Output;

import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.repowalker.FileChangeSummary;
import com.github.gumtreediff.tree.ITree;
import org.eclipse.jgit.lib.PersonIdent;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileChangeWriter implements ChangeWriter {

    private final BufferedWriter writer;

    public FileChangeWriter(String filePath) throws FileNotFoundException, UnsupportedEncodingException {
        this.writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(filePath), "utf-8"));
    }

    public void write(FileChangeSummary changeSummary) throws IOException {
        String commitHash = changeSummary.getCommit().getName();
        String parentCommitHash  = changeSummary.getParentCommit() != null
                ? changeSummary.getParentCommit().getName()
                : null;
        String commitMessage = changeSummary.getCommit().getShortMessage();

        // get time
        PersonIdent authorIdent = changeSummary.getCommit().getAuthorIdent();
        Date authorDate = authorIdent.getWhen();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        String dateString = dateFormat.format(authorDate);

        for (int i = 0; i < changeSummary.getChanges().size(); i++) {
            SourceCodeChange change = changeSummary.getChanges().get(i);
            ITree node = change.getNode();
            // get line info
            Object startLineObj = node.getMetadata("startline");
            Object endLineObj = node.getMetadata("endline");

            int startLine = startLineObj == null ? 0 : Integer.parseInt(startLineObj.toString());
            int endLine = startLineObj == null ? 0 : Integer.parseInt(endLineObj.toString());

            String line = String.format("%1$s;%2$s;%4$s;%5$s;%6$s;%7$s;%8$s;%9$s;%10$s;%11$s;%12$s;%13$s;%14$s;%15$s",
                    commitHash,
                    parentCommitHash,
                    dateString,
                    changeSummary.getSourceFileName(),
                    changeSummary.getDestinationFileName(),
                    change.getChangeType().toString(),
                    change.getChangeType().getCategory().toString(),
                    changeSummary.isTestCode(),
                    ITreeNodeHelper.getUniqueNodeName(change.getRootNode()),
                    ITreeNodeHelper.getUniqueNodeName(change.getTopLevelNode()),
                    SyntaxKindHelper.getByValue(change.getNode().getType()).toString(),
                    change.getActionType(),
                    startLine,
                    endLine);

            writer.write(line);
            writer.newLine();
        }

        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.flush();
        writer.close();
    }
}
