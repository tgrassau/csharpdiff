package at.gratoso.uni.repowalker.Output;

import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.Util.SyntaxKindHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.repowalker.FileChangeSummary;
import at.gratoso.uni.repowalker.Output.ChangeWriter;
import com.github.gumtreediff.tree.ITree;
import org.eclipse.jgit.lib.PersonIdent;

import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SqlChangeWriter implements ChangeWriter {
    private final String dbUrl;
    private final String dbUserName;
    private final String dbPassword;
    private final String dbName;

    private boolean isInitialized;
    private static Object lockObject = new Object();


    /**
     * Creates a new instance of SqlChangeWriter and initializes the database (create if not exists)
     *
     * @param dbUrl
     * @param dbUserName
     * @param dbPassword
     * @param dbName
     */
    public SqlChangeWriter(String dbUrl, String dbUserName, String dbPassword, String dbName) {
        this.dbUrl = dbUrl;
        this.dbUserName = dbUserName;
        this.dbPassword = dbPassword;
        this.dbName = dbName;

        initDatabase();
    }

    public void write(FileChangeSummary changeSummary) throws IOException {
        try {
            Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(
                    "INSERT INTO [dbo].[Changes] " +
                            "([CommitHash],[ParentCommitHash],[CommitMessage],[CommitTime]" +
                            ",[SourceFile],[DestinationFile],[ChangeType],[ChangeCategory]" +
                            ",[IsTestCode],[RootNodeName],[TopLevelNodeName],[NodeType]" +
                            ",[Position],[Length],[ActionType],[StartLine]" +
                            ",[EndLine]) " +
                    "VALUES " +
                            "(?,?,?,?" +
                            ",?,?,?,?" +
                            ",?,?,?,?" +
                            ",?,?,?,?" +
                            ",?)");

            addQueries(stmt, changeSummary);

            stmt.executeBatch();
            stmt.close();
            con.close();
        } catch (Exception e) {
            throw new IOException("Error while writing to database", e);
        }

    }

    private void addQueries(PreparedStatement statement, FileChangeSummary changeSummary) throws SQLException {
        try {

            String commitHash = changeSummary.getCommit().getName();
            String parentCommitHash  = changeSummary.getParentCommit() != null
                    ? changeSummary.getParentCommit().getName()
                    : null;
            String commitMessage = changeSummary.getCommit().getShortMessage();

            // get time
            PersonIdent authorIdent = changeSummary.getCommit().getAuthorIdent();
            Date authorDate = authorIdent.getWhen();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            String dateString = dateFormat.format(authorDate);


            for (int i = 0; i < changeSummary.getChanges().size(); i++) {
                SourceCodeChange change = changeSummary.getChanges().get(i);
                ITree node = change.getNode();
                // get line info
                Object startLineObj = node.getMetadata("startline");
                Object endLineObj = node.getMetadata("endline");

                int startLine = startLineObj == null ? 0 : Integer.parseInt(startLineObj.toString());
                int endLine = startLineObj == null ? 0 : Integer.parseInt(endLineObj.toString());

                statement.setString(1, commitHash);
                statement.setString(2, parentCommitHash);
                statement.setString(3, commitMessage);
                statement.setString(4, dateString);
                statement.setString(5, changeSummary.getSourceFileName());
                statement.setString(6, changeSummary.getDestinationFileName());
                statement.setString(7, change.getChangeType().toString());
                statement.setString(8, change.getChangeType().getCategory().toString());
                statement.setBoolean(9, changeSummary.isTestCode());
                statement.setString(10, ITreeNodeHelper.getUniqueNodeName(change.getRootNode()));
                statement.setString(11, ITreeNodeHelper.getUniqueNodeName(change.getTopLevelNode()));
                statement.setString(12, SyntaxKindHelper.getByValue(change.getNode().getType()).toString());
                statement.setInt(13, change.getNode().getPos());
                statement.setInt(14, change.getNode().getLength());
                statement.setString(15, change.getActionType());
                statement.setInt(16, startLine);
                statement.setInt(17, endLine);

                statement.addBatch();
            }

        } catch (Exception e) {
            if (e instanceof SQLException)
                throw e;

            System.err.println("file: " + changeSummary.getSourceFileName());
            e.printStackTrace();
        }

    }

    private Connection getConnection() throws SQLException {
        String connectionUrl = String.format("%s;database=%s;user=%s;password=%s", dbUrl, dbName, dbUserName, dbPassword);
        return DriverManager.getConnection(connectionUrl);
    }

    private void initDatabase() {
        if (!isInitialized) {
            synchronized (lockObject) {
                try {
                    // create database if necessary
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    Connection con = DriverManager.getConnection(
                            dbUrl,
                            dbUserName,
                            dbPassword);
                    Statement stmt = con.createStatement();
                    String sql = String.format("CREATE DATABASE %s", dbName);
                    stmt.execute(sql);

                    stmt.close();
                    con.close();

                    // create table if database was created
                    con = getConnection();
                    stmt = con.createStatement();

                    stmt.execute("CREATE TABLE [dbo].[Changes](\n" +
                            "\t[ID] [int] IDENTITY(1,1) NOT NULL,\n" +
                            "\t[CommitHash] [varchar](40) NOT NULL,\n" +
                            "\t[ParentCommitHash] [varchar](40) NULL,\n" +
                            "\t[CommitMessage] [varchar](8000) NOT NULL,\n" +
                            "\t[CommitTime] [datetime] NOT NULL,\n" +
                            "\t[SourceFile] [varchar](8000) NULL,\n" +
                            "\t[DestinationFile] [varchar](8000) NULL,\n" +
                            "\t[ChangeType] [varchar](100) NOT NULL,\n" +
                            "\t[ChangeCategory] [varchar](100) NOT NULL,\n" +
                            "\t[IsTestCode] [bit] NOT NULL,\n" +
                            "\t[RootNodeName] [varchar](8000) NOT NULL,\n" +
                            "\t[TopLevelNodeName] [varchar](8000) NOT NULL,\n" +
                            "\t[NodeType] [varchar] (100) NOT NULL,\n" +
                            "\t[Position] [int] NOT NULL,\n" +
                            "\t[Length] [int] NOT NULL,\n" +
                            "\t[ActionType] [char] (3) NOT NULL,\n" +
                            "\t[StartLine] [int] NOT NULL,\n" +
                            "\t[EndLine] [int] NOT NULL,\n" +
                            " CONSTRAINT [PK_Changes] PRIMARY KEY CLUSTERED \n" +
                            "(\n" +
                            "\t[ID] ASC\n" +
                            ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]\n" +
                            ") ON [PRIMARY]");
                    stmt.close();
                    con.close();
                } catch (SQLException sqlException) {
                    if (sqlException.getErrorCode() == 1007) {
                        System.out.println("Database already exists");
                    } else {
                        // Some other problems, e.g. Server down, no permission, etc
                        sqlException.printStackTrace();
                    }
                } catch (ClassNotFoundException e) {
                    // No driver class found!
                    System.out.println(e.getStackTrace());
                }

                isInitialized = true;
            }
        }
    }

    @Override
    public void close() throws IOException {
        // just nothing
    }
}
