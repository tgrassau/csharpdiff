package at.gratoso.uni.repowalker.Output;

import at.gratoso.uni.changeClassifier.Util.ITreeNodeHelper;
import at.gratoso.uni.changeClassifier.SourceCodeChange;
import at.gratoso.uni.repowalker.FileChangeSummary;
import at.gratoso.uni.repowalker.Output.ChangeWriter;

import java.io.IOException;

public class NullChangeWriter implements ChangeWriter {
    @Override
    public void write(FileChangeSummary changeSummary) {
        boolean isTestCode = changeSummary.isTestCode();

        for (int i = 0; i < changeSummary.getChanges().size(); i++) {
            SourceCodeChange change = changeSummary.getChanges().get(i);
            String topLevelNodeName = ITreeNodeHelper.getUniqueNodeName(change.getTopLevelNode());
        }


        // DO NOTHING ... blubb
    }

    @Override
    public void close() throws IOException {
        // just nothing
    }
}
