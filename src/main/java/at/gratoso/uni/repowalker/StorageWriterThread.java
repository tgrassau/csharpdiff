package at.gratoso.uni.repowalker;

import at.gratoso.uni.repowalker.Output.ChangeWriter;

import java.util.Queue;

public class StorageWriterThread extends Thread {
    private final Queue<FileChangeSummary> queue;
    private ChangeWriter writer;
    private boolean allChangesEnqueued = false;

    public StorageWriterThread(Queue<FileChangeSummary> queue, ChangeWriter writer) {
        this.queue = queue;
        this.writer = writer;
    }

    public void run() {
        while (queue.size() > 0 || !allChangesEnqueued) {
            while (!queue.isEmpty()) {
                FileChangeSummary changeSummary = queue.poll();

                try {
                    writer.write(changeSummary);
                } catch (Exception e) {
                    System.err.println("Error writing changes (" + changeSummary.getCommit().getName() + ")");
                    e.printStackTrace();
                }
            }

            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setAllChangesEnqueued() {
        this.allChangesEnqueued = true;
    }
}
