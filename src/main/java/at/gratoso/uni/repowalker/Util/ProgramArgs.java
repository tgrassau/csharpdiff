package at.gratoso.uni.repowalker.Util;

import java.io.InputStream;
import java.util.Properties;

public class ProgramArgs {

    private String repoPath;
    private String prodDir;
    private String testDir;

    private String dbUrl;
    private String dbPassword;
    private String dbUser;
    private String dbName;

    private String outputFile;

    private String[] dirsToExclude;

    public String getRepoPath() {
        return repoPath;
    }

    public String getProdDir() {
        return prodDir;
    }

    public String getDbUrl() {
        return dbUrl;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDbUser() {
        return dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String[] getDirsToExclude() {
        return dirsToExclude;
    }

    public String getTestDir() {
        return testDir;
    }

    public String getOutputFile() {
        return outputFile;
    }

    private ProgramArgs() {}

    public static ProgramArgs createFromProperties() throws Exception {
        ProgramArgs result = new ProgramArgs();
        InputStream inputStream = ProgramArgs.class.getClassLoader().getResourceAsStream("config.properties");

        try {
            Properties prop = new Properties();

            // load a properties file
            prop.load(inputStream);

            // get the property values
            result.repoPath = prop.getProperty("repoPath");
            result.prodDir = prop.getProperty("prodDir");
            result.testDir = prop.getProperty("testDir");
            String dirs = prop.getProperty("dirsToExclude");
            if (dirs != null && !dirs.equals(""))
                result.dirsToExclude = dirs.split(";");
            else
                result.dirsToExclude = new String[0];

            result.dbUrl = prop.getProperty("dbUrl");
            result.dbPassword = prop.getProperty("dbPassword");
            result.dbUser = prop.getProperty("dbUser");
            result.dbName = prop.getProperty("dbName");

            result.outputFile = prop.getProperty("outputFile");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }

        return result;
    }
}
