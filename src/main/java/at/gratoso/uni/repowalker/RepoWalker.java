package at.gratoso.uni.repowalker;

import at.gratoso.uni.repowalker.Output.ChangeWriter;
import at.gratoso.uni.repowalker.Output.FileChangeWriter;
import at.gratoso.uni.repowalker.Output.NullChangeWriter;
import at.gratoso.uni.repowalker.Output.SqlChangeWriter;
import at.gratoso.uni.repowalker.Util.ProgramArgs;
import org.apache.commons.lang3.time.StopWatch;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.StopWalkException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

public class RepoWalker {

    public static void main(String[] args) {
        ProgramArgs arguments = null;
        try {
            // get configuration from config.properties
            arguments = ProgramArgs.createFromProperties();
        } catch (Exception e) {
            System.out.println("Invalid properties file");
            e.printStackTrace();
        }

        Repository repository = null;

        try {
            repository = openRepository(arguments.getRepoPath());
        } catch (IOException e) {
            System.out.println("Repository not found!");
            return;
        }

        System.out.println(String.format("Start processing repository: %s", arguments.getRepoPath()));

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        if (repository != null) {
            try {
                // init change writer
                ChangeWriter writer = getChangeWriter(arguments);

                RepoWalker.analyzeCommits(
                        repository,
                        "HEAD",
                        arguments.getDirsToExclude(),
                        arguments.getProdDir(),
                        arguments.getTestDir(),
                        writer);

                // close writer
                writer.close();
            } catch (Exception e) {
                System.out.println("Error while reading repository");
                System.out.println(e.toString());
                e.printStackTrace();
            }
        }

        stopWatch.stop();
        System.out.println(String.format("Processing time: %s", formatMillis(stopWatch.getTime())));
    }

    /**
     * Analyzes all the commits of the configured repository
     *
     * @param repository
     * @param startCommit
     * @param dirsToExclude
     * @param prodDir
     * @param testDir
     * @param writer
     * @throws IOException
     * @throws GitAPIException
     * @throws InterruptedException
     */
    private static void analyzeCommits(Repository repository, String startCommit, String[] dirsToExclude, String prodDir, String testDir, ChangeWriter writer)
            throws IOException, GitAPIException, InterruptedException {

        // get commits
        Collection<RevCommit> commits = getCommits(repository, startCommit);

        // init shared queue
        Queue<FileChangeSummary> changeQueue = new ConcurrentLinkedQueue();

        // init threads
        Thread classifierThread = new ClassifierThread(repository, commits, changeQueue, prodDir, testDir, dirsToExclude);
        StorageWriterThread writerThread = new StorageWriterThread(changeQueue, writer);

        // start threads
        classifierThread.start();
        writerThread.start();

        // join classifier thread
        classifierThread.join();

        // as the classifier is finished, all changes are processed
        writerThread.setAllChangesEnqueued();

        // join writer thread
        writerThread.join();
    }

    /**
     * Creates the change writer depending on the configuration
     * @param arguments
     * @return
     * @throws Exception
     */
    private static ChangeWriter getChangeWriter(ProgramArgs arguments) throws Exception {

        // return sql change writer if all relevant info is provided
        if (!stringIsNullOrEmpty(arguments.getDbUrl())
                && !stringIsNullOrEmpty(arguments.getDbUser())
                && !stringIsNullOrEmpty(arguments.getDbPassword())
                && !stringIsNullOrEmpty(arguments.getDbName()))
            return new SqlChangeWriter(
                    arguments.getDbUrl(),
                    arguments.getDbUser(),
                    arguments.getDbPassword(),
                    arguments.getDbName());

        // return a file change writer if the output path is set
        if (!stringIsNullOrEmpty(arguments.getOutputFile()))
            return new FileChangeWriter(arguments.getOutputFile());

        // otherwise return a null change writer
        return new NullChangeWriter();
    }

    private static Repository openRepository(String repoPath) throws IOException {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();

        Repository repository = builder
                .readEnvironment()
                .setGitDir(new File(repoPath + "/.git"))
                .build();

        return repository;
    }

    private static Collection<RevCommit> getCommits(Repository repository, String startCommit) throws IOException {
        RevWalk walk = new RevWalk(repository);

        // set start commit
        walk.markStart(walk.parseCommit(repository.resolve(startCommit)));

        // set filter
        walk.setRevFilter(new RevFilter() {
            @Override
            public boolean include(RevWalk walker, RevCommit cmit) throws StopWalkException, IOException {
                if (cmit.getParentCount() > 1)
                    // this is a merge commit
                    return false;

                return true;
            }

            @Override
            public RevFilter clone() {
                return null;
            }
        });

        // extract commits
        List<RevCommit> commits = new Vector<>();
        RevCommit commit;
        while ((commit = walk.next()) != null) {
            commits.add(commit);
        }

        return commits;
    }

    private static String formatMillis(long millis) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static boolean stringIsNullOrEmpty( final String s ) {
        return s == null || s.trim().isEmpty();
    }
}
